const chartConfig = {
    palette: {
        default: [
            '#3B9ABD',
            '#A9E5A0',
            '#000D56',
            '#FEEB65',
            '#EB5E55',
        ],
    },
};

export default chartConfig;
