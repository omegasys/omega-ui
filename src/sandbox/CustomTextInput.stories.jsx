import React from 'react';
import CustomTextInput from './CustomTextInput';

export default {
    title: 'Input/CustomTextInput',
    component: CustomTextInput,
};

const Template = (args) => <CustomTextInput {...args} />;

export const Default = Template.bind({});
Default.args = {};
