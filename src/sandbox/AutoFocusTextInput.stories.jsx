import React from 'react';
import AutoFocusTextInput from './AutoFocusTextInput';

export default {
    title: 'Input/AutoFocusTextInput',
    component: AutoFocusTextInput,
};

const Template = (args) => <AutoFocusTextInput {...args} />;

export const Default = Template.bind({});
Default.args = {};
