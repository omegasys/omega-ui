import React from 'react';
import AutoCompleteMultiple from './AutoCompleteMultiple';

export default {
    title: 'Input/AutoCompleteMultiple',
    component: AutoCompleteMultiple,
};

// Please refer to preview.js and data.json for data initalization
const Template = (args, { loaded: { movies } }) => (
    <AutoCompleteMultiple {...args} options={movies} />
);

export const Default = Template.bind({});
Default.args = {
    optionKey: 'title',
    optionLabel: 'title',
    // defaultValue: [{ id: 1, label: 'Brand 1' }, { id: 2 }],
};
