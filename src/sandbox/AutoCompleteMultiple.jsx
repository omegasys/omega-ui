import React from 'react';
import Autocomplete from '@material-ui/lab/Autocomplete';
import TextField from '@material-ui/core/TextField';
import PropTypes from 'prop-types';
import OmListCheckBox from '../components/list-checkbox/OmListCheckBox';

export default function AutoCompleteMultiple(props) {
    const {
        multiple,
        color,
        label,
        options,
        optionKey,
        optionLabel,
        initialSelection,
        placeholder,
        limitTags,
        showTags,
        onChange,
        name,
        classes,
        ...rest
    } = props;

    const handleRenderInput = (params) => (
        <TextField
            label={label}
            placeholder={placeholder}
            {...params}
            name={name}
            fullWidth
        />
    );

    console.log(options);

    return (
        <Autocomplete
            multiple={multiple}
            options={options}
            ListboxComponent={React.forwardRef((props, ref) => (
                <div ref={ref}>
                    <OmListCheckBox
                        {...props}
                        data={options}
                        multiple={multiple}
                        optionKey={optionKey}
                        optionLabel={optionLabel}
                    />
                </div>
            ))}
            getOptionLabel={(opt) => opt[optionLabel]}
            renderInput={handleRenderInput}
            {...rest}
        />
    );
}

AutoCompleteMultiple.propTypes = {
    multiple: PropTypes.bool,
    optionKey: PropTypes.string.isRequired,
    optionLabel: PropTypes.string.isRequired,
    selectAllLabel: PropTypes.string,
    defaultValue: PropTypes.arrayOf(PropTypes.object),
};

AutoCompleteMultiple.defaultProps = {
    multiple: false,
    data: [],
    optionKey: 'id',
    defaultValue: [],
};
