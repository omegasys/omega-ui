export function intersection(a, b) {
    return a.filter((value) => b.indexOf(value) !== -1);
}

export function not(a, b) {
    return a.filter((value) => b.indexOf(value) === -1);
}

export function union(a, b) {
    return [...a, ...not(b, a)];
}

export const Wrapper = ({ children, condition, wrapper }) =>
    condition ? wrapper(children) : children;

export const generateRandomKey = () =>
    `${Math.random()}_${new Date().getTime()}`;
