// import React from 'react';
// import ReactDOM from 'react-dom';
// import './assets/main.css';
// import App from './App';
// import * as serviceWorker from './serviceWorker';
//
// ReactDOM.render(
//   <React.StrictMode>
//     <App />
//   </React.StrictMode>,
//   document.getElementById('root')
// );
//
// // If you want your app to work offline and load faster, you can change
// // unregister() to register() below. Note this comes with some pitfalls.
// // Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.unregister();

/** *** components **** */

export { default as OmButton } from './components/button/OmButton';
export { default as OmChart } from './components/chart/OmChart';
export { default as OmCheckbox } from './components/checkbox/OmCheckbox';
export { default as OmDatePicker } from './components/datetime-picker/OmDatePicker';
export { default as OmDateTimePicker } from './components/datetime-picker/OmDateTimePicker';
export { default as OmDialog } from './components/dialog/OmDialog';
export { default as OmDropDown } from './components/dropdown/OmDropDown';
export { default as OmDropzone } from './components/input/dragNDrop/OmDropzone';
export { default as OmListCheckBox } from './components/list-checkbox/OmListCheckBox';
export { default as OmLoadingOverlay } from './components/loading/OmLoadingOverlay';
export { default as OmPopover } from './components/popover/OmPopover';
export { default as OmSearchField } from './components/input/searchField/OmSearchField';
export { default as OmSwitch } from './components/input/switch/OmSwitch';
export { default as OmSimplePage } from './components/page/OmSimplePage';
export { default as OmTab } from './components/tab/OmTab';
export { default as OmDataTable } from './components/table/OmDataTable';
export { default as OmTable } from './components/table/OmTable';
export { default as OmTheme } from './components/theme/OmTheme';
export { default as OmTimePicker } from './components/datetime-picker/OmTimePicker';

// OmSelect is not ready yet
// export { default as OmSelect } from './components/select/OmSelect';

/** *** hooks **** */

export { default as useAnchorEl } from './hooks/useAnchorEl';
export { default as useDialog } from './hooks/useDialog';
export { default as useEffectUpdate } from './hooks/useEffectUpdate';
export { default as usePermissions } from './hooks/usePermission';
