import { useState } from 'react';

const usePermissions = () => {
    const [permissions, setPermissions] = useState(null);

    const hasPermission = (permission) => {
        if (!permissions) {
            const perms = sessionStorage.getItem('authPerms') == null
                ? []
                : JSON.parse(sessionStorage.getItem('authPerms'));
            setPermissions(perms);
            return perms.includes(permission);
        }
        return permissions.includes(permission);
    };

    return { hasPermission }
};

export default usePermissions;
