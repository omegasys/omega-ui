import { useState } from 'react';

const useAnchorEl = () => {
    const [anchorEl, setAnchorEl] = useState(null);

    const onAnchorClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const onAnchorClose = () => {
        setAnchorEl(null);
    };

    return { anchorEl, onAnchorClick, onAnchorClose };
};

export default useAnchorEl;
