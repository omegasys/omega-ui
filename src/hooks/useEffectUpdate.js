import { useEffect, useRef } from 'react';

/**
 * A custom hook that only fires on state updates and not fire for initial state
 * https://stackoverflow.com/questions/56247433/how-to-use-setstate-callback-on-react-hooks
 */

const useEffectUpdate = (effect, deps) => {
    const isFirstRender = useRef(true);

    useEffect(() => {
        if (!isFirstRender.current) {
            effect();
        }
    }, [deps, effect]);

    useEffect(() => {
        isFirstRender.current = false;
    }, []);
};

export default useEffectUpdate;
