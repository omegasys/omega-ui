import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';

/**
 * This component should be used for loading pages. Currently its a circular progress indicator,
 * in the future I think we can customize to OMEGA logo
 * @return {JSX.Element}
 * @constructor
 */
const OmLoadingOverlay = () => (
    <div className="om-flex om-justify-center om-align-items-center">
        <CircularProgress />
    </div>
);

export default OmLoadingOverlay;
