import React from 'react';
import OmListCheckBox, {
    OmListCheckBox as OmListCheckBoxComponent,
} from './OmListCheckBox';

export default {
    title: 'List Checkbox',
    component: OmListCheckBoxComponent,
    argTypes: { onClick: { action: 'clicked' } },
};

const Template = (args, { loaded: { brands } }) => (
    <OmListCheckBox {...args} data={brands} />
);

export const Default = Template.bind({});
Default.args = {
    multiple: true,
    optionKey: 'id',
    optionLabel: 'label',
    defaultValue: [{ id: 1, label: 'Brand 1' }, { id: 2 }],
};

export const NoDefaultValueSpecified = Template.bind({});
NoDefaultValueSpecified.args = {
    ...Default.args,
    defaultValue: null,
};
