import React, { memo, useEffect, useState } from 'react';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import CardHeader from '@material-ui/core/CardHeader';
import Card from '@material-ui/core/Card';
import PropTypes from 'prop-types';
import { Divider } from '@material-ui/core';
import * as _ from 'lodash';
import { intersection, not, union } from '../../utils/OmUtils';

export const OmListCheckBox = (props) => {
    const {
        defaultValue,
        variant,
        multiple,
        data,
        children,
        optionKey,
        optionLabel,
        selectAllLabel,
        onChange,
        ...rest
    } = props;

    // const defaultVal = defaultValue || _.minBy(data, optionKey);
    const defaultOption = getDefaultOption(
        data,
        defaultValue,
        optionKey,
    );
    const [checked, setChecked] = useState(defaultOption);
    const handleToggle = (value) => () => {
        const currentIndex = checked.indexOf(value);
        const newChecked = multiple ? [...checked] : [];

        if (currentIndex === -1) {
            newChecked.push(value);
        } else {
            newChecked.splice(currentIndex, 1);
        }
        setChecked(newChecked);
    };

    const numberOfChecked = (items) =>
        intersection(checked, items).length;

    const handleToggleAll = () => () => {
        const items = variant === 'ref' ? [...children] : [...data];
        if (numberOfChecked(items) === items.length) {
            setChecked(not(checked, items));
        } else {
            setChecked(union(checked, items));
        }
    };

    useEffect(() => {
        console.log(data);
    }, [data]);

    useEffect(() => {
        console.log(children);
    }, [children]);

    const renderOption = () => {
        const items = variant === 'ref' ? [...children] : [...data];
        const sorted = _.orderBy(items, optionLabel, 'asc');

        const checkAllSection = (sorted) => (
            <>
                <CardHeader
                    avatar={
                        <Checkbox
                            onClick={handleToggleAll(sorted)}
                            checked={
                                numberOfChecked(sorted) ===
                                    sorted.length &&
                                sorted.length !== 0
                            }
                            indeterminate={
                                numberOfChecked(sorted) !==
                                    sorted.length &&
                                numberOfChecked(sorted) !== 0
                            }
                            disableRipple
                            disabled={sorted.length === 0}
                            inputProps={{
                                'aria-label': 'all items selected',
                            }}
                        />
                    }
                    title={selectAllLabel}
                    subheader={`${numberOfChecked(sorted)} / 
          ${sorted.length}
           selected`}
                />
                <Divider />
            </>
        );

        return (
            <Card>
                {multiple && checkAllSection(items, 'Check All')}
                <List component="div" role="list">
                    {sorted.map((option, index) => {
                        const labelId = `checkbox-list-label-${option[optionKey]}`;
                        // return React.forwardRef((props, ref) => {
                        return (
                            <ListItem
                                role="listitem"
                                key={index}
                                dense
                                button
                                onClick={handleToggle(option)}
                            >
                                <ListItemIcon>
                                    <Checkbox
                                        checked={
                                            checked.indexOf(
                                                option,
                                            ) !== -1
                                        }
                                        tabIndex={-1}
                                        disableRipple
                                        inputProps={{
                                            'aria-labelledby': labelId,
                                        }}
                                    />
                                </ListItemIcon>
                                <ListItemText
                                    id={labelId}
                                    primary={
                                        variant === 'ref'
                                            ? option
                                            : option[optionLabel]
                                    }
                                />
                            </ListItem>
                        );
                        // });
                    })}
                </List>
            </Card>
        );
    };

    return <div {...rest}>{renderOption()}</div>;
};

OmListCheckBox.propTypes = {
    variant: PropTypes.string,
    multiple: PropTypes.bool,
    optionKey: PropTypes.string.isRequired,
    optionLabel: PropTypes.string.isRequired,
    selectAllLabel: PropTypes.string,
    defaultValue: PropTypes.arrayOf(PropTypes.object),
};

OmListCheckBox.defaultProps = {
    multiple: false,
    data: [],
    optionKey: 'id',
    defaultValue: [],
};

export default memo(OmListCheckBox);

function getDefaultOption(data, defaultValue, optionKey) {
    const defaultVal = [];
    // If the default value has not been specified, and there are more than 1 options,
    // the defualt option would be the minimum value based on the optionKey
    if (!defaultValue || defaultValue.length === 0) {
        defaultVal.push(_.minBy(data, optionKey));
        return defaultVal;
    }

    data.map((value) => {
        if (
            _.indexOf(
                _.map(defaultValue, optionKey),
                value[optionKey],
            ) !== -1
        )
            defaultVal.push(value);
    });
    return defaultVal;
}
