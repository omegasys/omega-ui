import React from 'react';
import PropTypes from 'prop-types';
import { KeyboardTimePicker, TimePicker } from '@material-ui/pickers';
import { Schedule } from '@material-ui/icons';

export const OmTimePicker = (props) => {
    const {
        name,
        inputRef,
        label,
        variant,
        ampm,
        value,
        handleDateChange,
        autoOk,
        disabledIcon,
        keyboardIcon,
        InputProps,
        ...rest
    } = props;

    return disabledIcon ? (
        <TimePicker
            autoOk={autoOk}
            label={label}
            variant={variant}
            ampm={ampm}
            value={value}
            onChange={handleDateChange}
            InputProps={{
                inputRef,
                name,
                ...InputProps,
            }}
            fullWidth
            {...rest}
        />
    ) : (
        <KeyboardTimePicker
            autoOk={autoOk}
            label={label}
            variant={variant}
            ampm={ampm}
            value={value}
            onChange={handleDateChange}
            keyboardIcon={keyboardIcon}
            InputProps={{
                inputRef,
                name,
                ...InputProps,
            }}
            fullWidth
            {...rest}
        />
    );
};

OmTimePicker.defaultProps = {
    variant: 'inline',
    autoOk: true,
    ampm: false,
    keyboardIcon: <Schedule />,
    disabledIcon: false,
};

OmTimePicker.propTypes = {
    name: PropTypes.string,
    inputRef: PropTypes.func,
    label: PropTypes.string,
    variant: PropTypes.string,
    ampm: PropTypes.bool,
    value: PropTypes.object,
    handleDateChange: PropTypes.func,
    autoOk: PropTypes.bool,
    disabledIcon: PropTypes.bool,
    keyboardIcon: PropTypes.node,
    InputProps: PropTypes.object,
};

export default OmTimePicker;
