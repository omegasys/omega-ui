const OmDatePickerStyles = (theme) => ({
    iconButton: {
        width: 36,
        height: 36,
        padding: 0,
        minWidth: 36,
        marginRight: 8,
    },
    icon: {
        fontSize: '2.375em',
    },
});

export default OmDatePickerStyles;
