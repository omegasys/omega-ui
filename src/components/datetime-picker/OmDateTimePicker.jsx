import React from 'react';
import PropTypes from 'prop-types';
import {
    DateTimePicker,
    KeyboardDateTimePicker,
} from '@material-ui/pickers';

export const OmDateTimePicker = (props) => {
    const {
        name,
        inputRef,
        label,
        variant,
        format,
        ampm,
        value,
        handleDateChange,
        autoOk,
        disabledIcon,
        InputProps,
        ...rest
    } = props;

    return disabledIcon ? (
        <DateTimePicker
            autoOk={autoOk}
            label={label}
            variant={variant}
            ampm={ampm}
            format={format}
            value={value}
            onChange={handleDateChange}
            InputProps={{
                inputRef,
                name,
                ...InputProps,
            }}
            fullWidth
            {...rest}
        />
    ) : (
        <KeyboardDateTimePicker
            autoOk={autoOk}
            label={label}
            variant={variant}
            ampm={ampm}
            format={format}
            value={value}
            onChange={handleDateChange}
            InputProps={{
                inputRef,
                name,
                ...InputProps,
            }}
            fullWidth
            {...rest}
        />
    );
};

OmDateTimePicker.defaultProps = {
    variant: 'inline',
    ampm: false,
    format: 'dd-MM-yyyy HH:mm',
    autoOk: true,
    disabledIcon: false,
};

OmDateTimePicker.propTypes = {
    name: PropTypes.string,
    inputRef: PropTypes.func,
    label: PropTypes.string,
    variant: PropTypes.string,
    format: PropTypes.string,
    ampm: PropTypes.bool,
    value: PropTypes.object,
    handleDateChange: PropTypes.func,
    autoOk: PropTypes.bool,
    disabledIcon: PropTypes.bool,
    InputProps: PropTypes.object,
};

export default OmDateTimePicker;
