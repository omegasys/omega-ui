import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { DatePicker, KeyboardDatePicker } from '@material-ui/pickers';
import withStyles from '@material-ui/core/styles/withStyles';
import OmDatePickerStyles from './OmDatePickerStyles';

export const OmDatePicker = (props) => {
    const {
        name,
        inputRef,
        value,
        format,
        variant,
        label,
        placeholder,
        handleDateChange,
        autoOk,
        disabledIcon,
        classes,
        color,
        ...rest
    } = props;

    return disabledIcon ? (
        <DatePicker
            autoOk={autoOk}
            value={value}
            variant={variant}
            format={format}
            label={label}
            color={color}
            placeholder={placeholder}
            onChange={(date) => handleDateChange(date)}
            fullWidth
            {...rest}
        />
    ) : (
        <KeyboardDatePicker
            autoOk={autoOk}
            value={value}
            variant={variant}
            format={format}
            label={label}
            color={color}
            placeholder={placeholder}
            onChange={(date) => handleDateChange(date)}
            fullWidth
            {...rest}
        />
    );
};

OmDatePicker.defaultProps = {
    variant: 'inline',
    format: 'dd-MM-yyyy',
    autoOk: true,
    disabledIcon: false,
};

OmDatePicker.propTypes = {
    name: PropTypes.string,
    value: PropTypes.object,
    variant: PropTypes.string,
    format: PropTypes.string,
    label: PropTypes.string,
    placeholder: PropTypes.string,
    handleDateChange: PropTypes.func,
    autoOk: PropTypes.bool,
    disabledIcon: PropTypes.bool,
    classes: PropTypes.object,
    color: PropTypes.string,
};

export default withStyles(OmDatePickerStyles)(memo(OmDatePicker));
