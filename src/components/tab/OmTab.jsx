import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

function TabPanel(props) {
    const { variant, children, value, index } = props;

    return (
        <Typography
            component="div"
            role="tabpanel"
            hidden={variant === 'multiple' && value !== index}
            id={`om-tabpanel-${index}`}
            aria-labelledby={`om-tab-${index}`}
        >
            {(variant === 'single' || value === index) && (
                <Box>{children}</Box>
            )}
        </Typography>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
    variant: PropTypes.string.isRequired,
};

function a11yProps(index) {
    return {
        id: `om-tab-${index}`,
        'aria-controls': `om-tabpanel-${index}`,
    };
}

const useStyles = makeStyles((theme) => ({
    appBar: {
        color: '#545454',
        backgroundColor: 'transparent',
        borderBottom: '1px solid #DDDDDD',
    },
    root: {
        minHeight: 0,
    },
    indicator: {
        backgroundColor: '#3B9ABD',
        height: '2px',
    },
    tab: {
        outline: 'none !important',
        backgroundColor: 'transparent',
        minWidth: 0,
        minHeight: 0,
        padding: '0 12px',
        fontSize: 20,
        lineHeight: '26px',
        textTransform: 'initial',
    },
}));

function renderMultiplePage(pages, value) {
    return pages.map((page, key) => (
        <TabPanel
            variant="multiple"
            key={key}
            value={value}
            index={key}
        >
            {page}
        </TabPanel>
    ));
}

function renderSinglePage(page, value) {
    return (
        <TabPanel variant="single" value={value} index={0}>
            {page}
        </TabPanel>
    );
}

function OmTab(props) {
    const classes = useStyles();
    const [value, setValue] = useState(0);

    const handleChange = (event, newValue) => {
        setValue(newValue);
        if (props.variant === 'single' && props.tabs[newValue]) {
            props.onTabChange(props.tabs[newValue].param);
        }
    };

    return (
        <div className={props.className}>
            <AppBar
                position="static"
                classes={{ root: classes.appBar }}
                elevation={0}
            >
                <Tabs
                    value={value}
                    onChange={handleChange}
                    indicatorColor="primary"
                    textColor="inherit"
                    variant="standard"
                    aria-label="omega tabs"
                    classes={{
                        root: classes.root,
                        indicator: classes.indicator,
                    }}
                >
                    {props.tabs.map((tab, key) => (
                        <Tab
                            key={key}
                            label={tab.label}
                            disabled={tab.readOnly}
                            classes={{ root: classes.tab }}
                            {...a11yProps(key)}
                        />
                    ))}
                </Tabs>
            </AppBar>
            <div className="om-mt-4 om-p-4">
                {props.variant === 'multiple'
                    ? renderMultiplePage(props.children, value)
                    : renderSinglePage(props.child, value)}
            </div>
        </div>
    );
}

OmTab.propTypes = {
    id: PropTypes.string,
    variant: PropTypes.string,
    className: PropTypes.string,
    tabs: PropTypes.array.isRequired,
    child: PropTypes.object,
    children: PropTypes.array,
    onTabChange: PropTypes.func,
};

OmTab.defaultProps = {
    variant: 'multiple',
    tabs: [],
};

export default OmTab;
