import { Button, Fab, IconButton } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import React from 'react';

const useStyles = makeStyles({
    root: {
        '&:focus': {
            outline: 'none',
        },
    },
    fab: {
        '&:focus': {
            outline: 'none',
        },
    },
    squareIcon: {
        '&:focus': {
            outline: 'none',
        },
        padding: '0 8px',
        minWidth: 0,
        borderWidth: 3,
        fontSize: '1em',
    },
});

export function OmButton(props) {
    const classes = useStyles();
    const {
        color,
        variant,
        size,
        icon,
        fab,
        square,
        children,
        ...others
    } = props;
    const styleBorderRadius = square ? 0 : 8; // square | rounded

    if (fab) {
        return (
            <Fab
                classes={{ root: classes.fab }}
                size={size}
                color={color}
                {...others}
            >
                {icon ? (
                    <i className="material-icons">{name}</i>
                ) : (
                    name
                )}
            </Fab>
        );
    }
    if (square) {
        return (
            <Button
                classes={{ root: classes.squareIcon }}
                variant="outlined"
                color={color}
                {...others}
                size={size}
            >
                {children}
            </Button>
        );
    }
    if (icon) {
        return (
            <IconButton
                classes={{ root: classes.root }}
                color={color}
                {...others}
                size={size}
            >
                <i className="material-icons">{name}</i>
            </IconButton>
        );
    }

    return (
        <Button
            classes={{ root: classes.root }}
            style={{ borderRadius: styleBorderRadius }}
            variant={variant}
            color={color}
            size={size}
            {...others}
        >
            {children}
        </Button>
    );
}

OmButton.propTypes = {
    name: PropTypes.string,
    variant: PropTypes.string,
    color: PropTypes.string,
    size: PropTypes.string,
    square: PropTypes.bool,
    icon: PropTypes.bool,
    fab: PropTypes.bool,
    children: PropTypes.node,
};

OmButton.defaultProps = {
    variant: 'outlined', // outlined | contained | text
    color: 'primary', // primary | secondary | default
    square: false,
    icon: false,
    fab: false,
};

export default React.memo(OmButton);
