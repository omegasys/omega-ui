import React, { memo } from 'react';
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';

const OmPaperContainer = (props) => {
    const { variant, square, children, ...rest } = props;
    return (
        <Paper variant={variant} square={square} {...rest}>
            {children}
        </Paper>
    );
};

OmPaperContainer.defaultProps = {
    variant: 'outlined',
    square: true,
    children: null,
};

OmPaperContainer.propTypes = {
    variant: PropTypes.string,
    square: PropTypes.bool,
    children: PropTypes.node,
};

export default memo(OmPaperContainer);
