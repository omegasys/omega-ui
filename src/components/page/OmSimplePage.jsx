import React, { memo } from 'react';
import PropTypes from 'prop-types';

const OmSimplePage = (props) => {
    const { className, children, ...rest } = props;
    return (
        <div className={className} {...rest}>
            {children}
        </div>
    );
};

OmSimplePage.defaultProps = {
    className: 'om-p-2',
};

OmSimplePage.propTypes = {
    children: PropTypes.node,
    className: PropTypes.string,
};
export default memo(OmSimplePage);
