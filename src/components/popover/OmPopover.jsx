import React from 'react';
import PropTypes from 'prop-types';
import Popover from '@material-ui/core/Popover';

const OmPopover = (props) => {
    const { handleClose, anchorEl, component } = props;
    const open = Boolean(anchorEl);
    const id = open ? 'om-popover' : undefined;

    return (
        <Popover
            id={id}
            open={open}
            anchorEl={anchorEl}
            onClose={handleClose}
            anchorOrigin={{
                vertical: 'center',
                horizontal: 'center',
            }}
            transformOrigin={{
                vertical: 'top',
                horizontal: 'center',
            }}
            PaperProps={{
                style: {
                    padding: '16px',
                },
            }}
        >
            {component}
        </Popover>
    );
};

OmPopover.defaultProps = {
    anchorEl: null,
    handleClose: undefined,
};

OmPopover.propTypes = {
    anchorEl: PropTypes.object,
    handleClose: PropTypes.func,
    component: PropTypes.element.isRequired,
};

export default OmPopover;
