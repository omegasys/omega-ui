import themesConfig from '../../configs/ThemesConfig';

export const ThemeOverrides = (themeName) => ({
    MuiInput: {
        underline: {
            '&::before': {
                borderBottomWidth: 2,
                borderBottomColor:
                    themesConfig[themeName].palette.secondary.main,
            },
        },
    },
    MuiFab: {
        root: {
            '&.Mui-disabled': {
                color: '#bdbdbd',
                backgroundColor: '#e0e0e0',
            },
        },
    },
    MuiOutlinedInput: {
        root: {
            borderRadius: 12,
        },
    },
    MuiPaper: {
        rounded: {
            borderRadius: 12,
        },
    },
    MuiTableCell: {
        stickyHeader: {
            whiteSpace: 'nowrap'
        }
    },
    MuiAutocomplete: {
        popper: {
            width: '100%',
        },
        paper: {
            borderRadius: 0,
        },
        listbox: {
            maxHeight: 300,
            overflow: 'auto',
            boxShadow:
                '0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23)',
        },
    },
    MuiDialogTitle: {
        root: {
            cursor: 'move',
        },
    },
});
