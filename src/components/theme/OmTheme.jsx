import React from 'react';
import PropTypes from 'prop-types';
import { ThemeProvider } from '@material-ui/styles';
import { createMuiTheme } from '@material-ui/core/styles';
import themesConfig from '../../configs/ThemesConfig';
import chartConfig from '../../configs/ChartConfig';
import { ThemeProps } from './OmTheme-Props';
import { ThemeOverrides } from './OmTheme-Overrides';

function OmTheme(props) {
    const { themeName, children } = props;
    const mainTheme = createMuiTheme({
        palette: themesConfig[themeName].palette,
        chartPalette: chartConfig.palette.default,
        props: ThemeProps(themeName),
        overrides: ThemeOverrides(themeName),
    });

    return (
        <ThemeProvider theme={mainTheme}>{children}</ThemeProvider>
    );
}

OmTheme.propTypes = {
    themeName: PropTypes.string.isRequired,
    children: PropTypes.node.isRequired,
};

export default React.memo(OmTheme);
