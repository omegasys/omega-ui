export const ThemeProps = (themeName) => ({
    // Name of the component
    MuiCard: {
        // The default props to change
        elevation: 3,
    },
    MuiGrid: {
        spacing: 2,
    },
    MuiTextField: {
        variant: 'outlined',
        margin: 'dense',
        size: 'small',
    },
    MuiTable: {
        size: 'small',
    },
    MuiAutocomplete: {
        size: 'small',
    },
});
