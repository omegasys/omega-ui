const OmChartStyle = (theme) => ({
    label1: {
        color: theme.chartPalette[0],
        transform: 'rotate(-90deg)',
        fontSize: 16,
        fontWeight: 'bold',
    },
    label2: {
        color: theme.chartPalette[1],
        transform: 'rotate(-90deg)',
        fontSize: 16,
        fontWeight: 'bold',
    },
});

export default OmChartStyle;
