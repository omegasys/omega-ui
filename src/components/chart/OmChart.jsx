import React, { memo } from 'react';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import OmChartStyle from './OmChartStyle';
import OmChartLine from './OmChart-line';
import OmChartBar from './OmChart-bar';
import OmChartPie from './OmChart-pie';
import { OmLoadingOverlay } from '@omegasys/omega-ui/core';

export const OmChart = (props) => {
    const { variant, data, height, graphConfig, loading } = props;

    let chart;
    switch (variant) {
        case 'bar':
            chart = (
                <OmChartBar
                    data={data}
                    height={height}
                    options={graphConfig}
                />
            );
            break;
        case 'line':
            chart = <OmChartLine data={data} options={graphConfig} />;
            break;
        case 'pie':
            chart = <OmChartPie data={data} options={graphConfig} />;
            break;
    }
    return (
        <div className={props.className}>
            {loading ? <OmLoadingOverlay /> : chart}
        </div>
    );
};

OmChart.propTypes = {
    variant: PropTypes.string.isRequired,
    className: PropTypes.string,
    height: PropTypes.number,
    data: PropTypes.shape({
        xAxis: PropTypes.shape({
            legend: PropTypes.string,
            labels: PropTypes.array.isRequired,
        }),
        data: PropTypes.arrayOf(
            PropTypes.shape({
                legend: PropTypes.string.isRequired,
                type: PropTypes.string.isRequired,
                format: PropTypes.string,
                symbol: PropTypes.string,
                values: PropTypes.array.isRequired,
            }),
        ),
    }).isRequired,
    graphConfig: PropTypes.object,
};

OmChart.defaultProps = {
    variant: 'bar',
    height: 100,
    data: {},
    graphConfig: {},
};

export default withStyles(OmChartStyle)(memo(OmChart));
