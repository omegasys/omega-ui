import React from 'react';
import PropTypes from 'prop-types';
import { Pie } from 'react-chartjs-2';
import withStyles from '@material-ui/core/styles/withStyles';
import OmChartStyle from './OmChartStyle';

function OmChartPie(props) {
    const { data, options } = props;
    return <Pie data={data} options={options} />;
}

OmChartPie.propTypes = {
    data: PropTypes.object,
    options: PropTypes.object,
};

OmChartPie.defaultProps = {};

export default withStyles(OmChartStyle)(OmChartPie);
