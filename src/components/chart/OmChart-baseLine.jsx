const BaseLinePlugin = (chartInstance, easing) => {
    const { ctx } = chartInstance.chart;
    const height = chartInstance.chart.scales['y-axis-0'].bottom;
    const leftX = chartInstance.chart.scales['x-axis-0'].left;
    const rightX = chartInstance.chart.scales['x-axis-0'].right;
    const cornerRadius = 5;

    ctx.beginPath();

    ctx.lineJoin = 'round';
    ctx.lineWidth = cornerRadius;
    ctx.fillStyle = '#707070';
    ctx.strokeStyle = '#707070';
    ctx.strokeRect(leftX, height + 10, rightX - leftX, 5);
    ctx.fillRect(leftX, height + 10, rightX - leftX, 5);
};

export default BaseLinePlugin;
