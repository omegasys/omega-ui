import React from 'react';
import PropTypes from 'prop-types';
import { Bar } from 'react-chartjs-2';
import withStyles from '@material-ui/core/styles/withStyles';
import OmChartStyle from './OmChartStyle';
import BaseLinePlugin from './OmChart-baseLine';

const plugins = [
    {
        afterDraw: BaseLinePlugin,
    },
];

const defaultOptions = {
    responsive: true,
    tooltips: {
        mode: 'label',
        backgroundColor: '#F8FDFF',
        bodyFontColor: '#202020',
        titleFontColor: '#202020',
        titleAlign: 'right',
    },
    elements: {
        line: {
            fill: false,
        },
    },
    legend: {
        display: false,
    },
    scales: {
        xAxes: [
            {
                display: true,
                id: 'x-axis-0',
                gridLines: {
                    display: false,
                    drawBorder: false,
                    color: '#707070',
                },
                ticks: {
                    padding: 20,
                },
                labels: [
                    '01/05',
                    '02/05',
                    '03/05',
                    '04/05',
                    '05/05',
                    '06/05',
                    '07/05',
                ],
            },
        ],
        yAxes: [
            {
                type: 'linear',
                display: true,
                position: 'right',
                id: 'y-axis-0',
                gridLines: {
                    display: false,
                    drawBorder: false,
                },
                labels: {
                    show: true,
                },
            },
            {
                type: 'linear',
                display: true,
                position: 'left',
                id: 'y-axis-1',
                gridLines: {
                    display: false,
                    drawBorder: false,
                },
                labels: {
                    show: true,
                },
            },
        ],
    },
};

function constructOptions(labels) {
    const options = { ...defaultOptions };
    options.scales.xAxes.labels = labels;
    return options;
}

function OmChartBar(props) {
    const { labels, data, classes } = props;

    const options = constructOptions(labels);

    return (
        <div className="om-w-full om-flex om-flex-row om-items-center">
            <div className={classes.label1}>Revenue</div>
            <div className="om-flex-1 om-flex om-flex-col om-items-center">
                <div className="om-text-2xl om-font-bold">
                    Revenue Vs Hold
                </div>
                <Bar
                    data={data}
                    options={options}
                    plugins={plugins}
                />
            </div>
            <div className={classes.label2}>Hold</div>
        </div>
    );
}

OmChartBar.propTypes = {
    labels: PropTypes.string,
    data: PropTypes.object,
    options: PropTypes.object,
    classes: PropTypes.object,
};

OmChartBar.defaultProps = {};

export default withStyles(OmChartStyle)(OmChartBar);
