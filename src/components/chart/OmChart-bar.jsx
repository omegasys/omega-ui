import React from 'react';
import PropTypes from 'prop-types';
import { Bar } from 'react-chartjs-2';
import withStyles from '@material-ui/core/styles/withStyles';
import OmChartStyle from './OmChartStyle';
import BaseLinePlugin from './OmChart-baseLine';
import chartConfig from '../../configs/ChartConfig';

const plugins = [
    {
        afterDraw: BaseLinePlugin,
    },
];

const defaultOptions = {
    responsive: true,
    maintainAspectRatio: false,
    tooltips: {
        mode: 'index',
        backgroundColor: '#F8FDFF',
        bodyFontColor: '#202020',
        titleFontColor: '#202020',
        titleAlign: 'right',
        callbacks: {
            label(tooltipItem, data) {
                let dataLabel = `${
                    data.datasets[tooltipItem.datasetIndex].label
                }: `;
                const value =
                    data.datasets[tooltipItem.datasetIndex].data[
                        tooltipItem.index
                    ];
                let displayValue = value;
                // console.log(data.datasets[tooltipItem.datasetIndex])
                switch (
                    data.datasets[tooltipItem.datasetIndex].format
                ) {
                    case 'currency':
                        displayValue =
                            data.datasets[tooltipItem.datasetIndex]
                                .curencySymbol +
                            value.toLocaleString();
                        break;
                    case 'percentage':
                        displayValue = `${value.toFixed(0)}%`;
                }
                dataLabel += displayValue;
                // return the text to display on the tooltip
                return dataLabel;
            },
        },
    },
    elements: {
        line: {
            fill: false,
        },
    },
    legend: {
        display: false,
    },
    scales: {
        xAxes: [
            {
                display: true,
                id: 'x-axis-0',
                gridLines: {
                    display: false,
                    drawBorder: false,
                    color: '#707070',
                },
                ticks: {
                    padding: 20,
                },
                labels: [],
            },
        ],
        yAxes: [],
    },
};

function constructOptions(raw) {
    const options = { ...defaultOptions };
    options.scales.xAxes[0].labels = raw.xAxis.labels;

    const yAxes = [];
    raw.data.forEach((set, index) => {
        const yAxis = {
            type: 'linear',
            display: true,
            position: index > 0 ? 'right' : 'left',
            id: `y-axis-${index}`,
            gridLines: {
                display: false,
                drawBorder: false,
            },
            labels: {
                show: true,
            },
            ticks: {
                beginAtZero: true,
                callback(value, index, values) {
                    switch (set.format) {
                        case 'currency': {
                            if (parseInt(value) >= 1000) {
                                return (
                                    set.symbol +
                                    value
                                        .toString()
                                        .replace(
                                            /\B(?=(\d{3})+(?!\d))/g,
                                            ',',
                                        )
                                );
                            }
                            return set.symbol + value;
                        }
                        case 'percentage':
                            return `${value.toFixed(2)}%`;
                    }
                },
            },
        };
        yAxes.push(yAxis);
    });
    options.scales.yAxes = yAxes;
    return options;
}

function constructData(raw) {
    const parsed = {
        datasets: [],
    };

    raw.data.forEach((set, index) => {
        let dataOption;
        if (set.type === 'bar') {
            dataOption = {
                label: set.legend,
                type: 'bar',
                data: set.values,
                format: set.format,
                curencySymbol: set.symbol,
                order: set.order,
                fill: false,
                backgroundColor: chartConfig.palette.default[index],
                borderColor: chartConfig.palette.default[index],
                hoverBackgroundColor:
                    chartConfig.palette.default[index],
                hoverBorderColor: chartConfig.palette.default[index],
                yAxisID: `y-axis-${index}`,
            };
        } else if (set.type === 'line') {
            dataOption = {
                label: set.legend,
                type: 'line',
                lineTension: 0,
                data: set.values,
                format: set.format,
                curencySymbol: set.symbol,
                order: set.order,
                fill: false,
                borderColor: chartConfig.palette.default[index],
                backgroundColor: chartConfig.palette.default[index],
                pointBorderColor: chartConfig.palette.default[index],
                pointBackgroundColor:
                    chartConfig.palette.default[index],
                pointBorderWidth: 11,
                pointHoverBorderWidth: 11,
                pointHoverBackgroundColor:
                    chartConfig.palette.default[index],
                pointHoverBorderColor:
                    chartConfig.palette.default[index],
                yAxisID: `y-axis-${index}`,
            };
        }
        parsed.datasets.push(dataOption);
    });
    console.log(parsed);
    return parsed;
}

export function OmChartBar(props) {
    const { data, classes, height, options } = props;

    const defaultOptions = constructOptions(data);
    const parsed = constructData(data);

    return (
        <div
            className="om-w-full om-flex om-flex-row om-items-center"
            style={{ height: height + 50 }}
        >
            <div className={classes.label1}>
                {data.data[0] && data.data[0].legend}
            </div>
            <div className="om-flex-1 om-flex om-flex-col om-items-center">
                <div className="om-text-lg om-font-bold">
                    {data.data[0] && data.data[0].legend}
                    {data.data[1] && ` Vs ${data.data[1].legend}`}
                </div>
                <div className="om-w-full" style={{ height }}>
                    <Bar
                        data={parsed}
                        options={{ ...defaultOptions, ...options }}
                        plugins={plugins}
                    />
                </div>
                <div className="om-text-lg om-font-bold">
                    {data.xAxis.legend}
                </div>
            </div>
            <div className={classes.label2}>
                {data.data[1] && data.data[1].legend}
            </div>
        </div>
    );
}

OmChartBar.propTypes = {
    classes: PropTypes.object,
    data: PropTypes.object,
    height: PropTypes.number,
    options: PropTypes.object,
};

OmChartBar.defaultProps = {};

export default withStyles(OmChartStyle)(OmChartBar);
