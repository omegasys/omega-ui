import React from 'react';
import PropTypes from 'prop-types';
import { Line } from 'react-chartjs-2';
import withStyles from '@material-ui/core/styles/withStyles';
import OmChartStyle from './OmChartStyle';
import BaseLinePlugin from './OmChart-baseLine';

const plugins = [
    {
        afterDraw: BaseLinePlugin,
    },
];

function OmChartLine(props) {
    const { data, options } = props;
    return <Line data={data} options={options} />;
}

OmChartLine.propTypes = {
    data: PropTypes.object,
    options: PropTypes.object,
};

OmChartLine.defaultProps = {};

export default withStyles(OmChartStyle)(OmChartLine);
