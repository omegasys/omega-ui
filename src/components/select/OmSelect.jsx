import React, { memo } from 'react';
import PropTypes from 'prop-types';
import Autocomplete from '@material-ui/lab/Autocomplete';
import TextField from '@material-ui/core/TextField';
import withStyles from '@material-ui/core/styles/withStyles';
import OmSelectStyle from './OmSelectStyle';
import { OmListCheckBox } from '../list-checkbox/OmListCheckBox';

export const OmSelect = (props) => {
    const {
        multiple,
        color,
        label,
        options,
        optionKey,
        optionLabel,
        initialSelection,
        placeholder,
        limitTags,
        showTags,
        onChange,
        name,
        classes,
        ...rest
    } = props;

    const handleRenderInput = (params) => (
        <TextField
            label={label}
            placeholder={placeholder}
            {...params}
            name={name}
            fullWidth
        />
    );

    return (
        <Autocomplete
            multiple={multiple}
            options={options}
            ListboxComponent={React.forwardRef((props, ref) => (
                <div ref={ref}>
                    <OmListCheckBox
                        {...props}
                        // data={options}
                        variant="ref"
                        multiple={multiple}
                        optionKey={optionKey}
                        optionLabel={optionLabel}
                    />
                </div>
            ))}
            getOptionLabel={(opt) => opt[optionLabel]}
            renderInput={handleRenderInput}
            {...rest}
        />
    );
};

OmSelect.propTypes = {
    name: PropTypes.string,
    multiple: PropTypes.bool,
    label: PropTypes.string,
    placeholder: PropTypes.string,
    color: PropTypes.string,
    options: PropTypes.arrayOf(PropTypes.object).isRequired,
    optionKey: PropTypes.string.isRequired,
    optionLabel: PropTypes.string.isRequired,
    initialSelection: PropTypes.arrayOf(PropTypes.object),
    limitTags: PropTypes.number,
    showTags: PropTypes.bool,
    disabled: PropTypes.bool,
    onChange: PropTypes.func,
    classes: PropTypes.object,
};

OmSelect.defaultProps = {
    multiple: false,
    options: [],
    optionKey: '',
    optionLabel: '',
    initialSelection: [],
    color: 'primary',
    limitTags: 1,
    showTags: false,
};

export default withStyles(OmSelectStyle)(memo(OmSelect));
