import React, { memo, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import Chip from '@material-ui/core/Chip';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { createFilterOptions } from '@material-ui/lab';
import withStyles from '@material-ui/core/styles/withStyles';
import OmCheckbox from '../checkbox/OmCheckbox';
import OmSelectStyle from './OmSelectStyle';

export const OmMultiSelect = (props) => {
    const listItemStyle = {
        color: 'grey',
    };

    const {
        color,
        label,
        options,
        key,
        displayField,
        placeholder,
        limitTags,
        showTags,
        onChange,
        value,
        name,
        classes,
        ...rest
    } = props;
    const selectAllLabel = 'Select All';
    const [selectedOptions, setSelectedOptions] = useState(
        props.value ? props.value : [],
    );

    const toggleSelectAll = () => {
        setSelectedOptions(isAllSelected() ? [] : options);
    };

    const isAllSelected = () =>
        options.length === selectedOptions.length;

    useEffect(() => {
        setSelectedOptions(props.value ? props.value : []);
    }, [props.value]);

    useEffect(() => {
        console.log(options);
    }, [options]);

    const getOptLabel = (opt) => opt[displayField];
    // array of objects can cause unmatch of value and option, which generates lots of warnings
    const handleObjectMatch = (source, target) =>
        source[key] === target[key];

    const handleRenderTags = showTags
        ? (values, getTagProps) =>
              values.map((opt, idx) => (
                  <Chip
                      {...getTagProps({ idx })}
                      key={idx}
                      label={getOptLabel(opt)}
                  />
              ))
        : (values, getTagProps) => {
              if (values.length <= limitTags) {
                  const tags = values.map((opt, idx) =>
                      getOptLabel(opt),
                  );
                  return intersep(tags, ', ');
              }
              return `${values.length} Selected`;
          };

    const handleChange = (event, selectedOpts, reason) => {
        if (
            reason === 'select-option' ||
            reason === 'remove-option'
        ) {
            if (
                selectedOpts.find(
                    (opt) => getOptLabel(opt) === selectAllLabel,
                )
            ) {
                toggleSelectAll();
                return onChange(selectedOptions);
            }
            setSelectedOptions(selectedOpts);
            return onChange(selectedOpts);
        }
        if (reason === 'clear') {
            setSelectedOptions([]);
            return onChange([]);
        }
    };

    const handleRenderOption = (opt, { selected }) => {
        const selectAllProps =
            getOptLabel(opt) === selectAllLabel
                ? { checked: isAllSelected() }
                : {};
        return (
            <>
                <OmCheckbox
                    color={color}
                    checked={selected}
                    {...selectAllProps}
                />
                <span style={listItemStyle}>{getOptLabel(opt)}</span>
            </>
        );
    };

    const handleRenderInput = (params) => (
        <TextField
            label={label}
            placeholder={placeholder}
            {...params}
            name={name}
            fullWidth
        />
    );

    const filter = createFilterOptions();
    const handleFilterOptions = (opts, params) => {
        const filtered = filter(opts, params);
        const selectAll = displayField
            ? { [displayField]: selectAllLabel }
            : selectAllLabel;
        return [selectAll, ...filtered];
    };

    return (
        <Autocomplete
            multiple
            disableCloseOnSelect
            limitTags={limitTags}
            onChange={handleChange}
            options={options || []}
            value={selectedOptions}
            classes={{
                inputRoot: classes.inputContainer,
                popper: classes.popper,
            }}
            getOptionLabel={(opt) => getOptLabel(opt)}
            getOptionSelected={handleObjectMatch}
            ListboxProps={{
                style: {
                    maxHeight: 200,
                    overflow: 'auto',
                    boxShadow:
                        '0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23)',
                },
            }}
            renderTags={handleRenderTags}
            renderOption={handleRenderOption}
            renderInput={handleRenderInput}
            filterOptions={handleFilterOptions}
            {...rest}
        />
    );
};

function intersep(arr, sep) {
    if (arr.length === 0) {
        return [];
    }
    return arr
        .slice(1)
        .reduce((str, x, i) => str.concat([sep, x]), [arr[0]]);
}

OmMultiSelect.propTypes = {
    name: PropTypes.string,
    label: PropTypes.string,
    placeholder: PropTypes.string,
    color: PropTypes.string,
    options: PropTypes.arrayOf(PropTypes.object).isRequired,
    value: PropTypes.array,
    key: PropTypes.string.isRequired,
    displayField: PropTypes.string.isRequired,
    limitTags: PropTypes.number,
    showTags: PropTypes.bool,
    disabled: PropTypes.bool,
    onChange: PropTypes.func,
    classes: PropTypes.object,
};

OmMultiSelect.defaultProps = {
    options: [],
    value: [],
    key: '',
    displayField: '',
    color: 'primary',
    limitTags: 1,
    showTags: false,
};

export default withStyles(OmSelectStyle)(memo(OmMultiSelect));
