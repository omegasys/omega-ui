const OmSelectStyle = (theme) => ({
    root: {
        '&:focus': {
            outline: 'none',
        },
    },
    inputContainer: {
        '&::before': {
            borderBottomWidth: 2,
            borderBottomColor: theme.palette.secondary.main,
        },
    },
    popper: {
        width: 'auto !important',
    },
});

export default OmSelectStyle;
