import React, { memo } from 'react';
import PropTypes from 'prop-types';
import IconButton from '@material-ui/core/IconButton';
import {
    FirstPage,
    KeyboardArrowLeft,
    KeyboardArrowRight,
    LastPage,
} from '@material-ui/icons';
import withStyles from '@material-ui/core/styles/withStyles';
import OmTableStyle from '../OmTableStyle';

const OmTablePaginationActions = (props) => {
    const { count, page, rowsPerPage, onChangePage, classes } = props;

    const handleFirstPageButtonClick = () => {
        onChangePage(0);
    };

    const handleBackButtonClick = () => {
        onChangePage(page - 1);
    };

    const handleNextButtonClick = () => {
        onChangePage(page + 1);
    };

    const handleLastPageButtonClick = () => {
        onChangePage(Math.max(0, Math.ceil(count / rowsPerPage) - 1));
    };

    return (
        <div className={classes.tablePaginationActionsRoot}>
            <IconButton
                onClick={handleFirstPageButtonClick}
                disabled={page === 0}
            >
                <FirstPage />
            </IconButton>
            <IconButton
                onClick={handleBackButtonClick}
                disabled={page === 0}
            >
                <KeyboardArrowLeft />
            </IconButton>
            <IconButton
                onClick={handleNextButtonClick}
                disabled={page >= Math.ceil(count / rowsPerPage) - 1}
            >
                <KeyboardArrowRight />
            </IconButton>
            <IconButton
                onClick={handleLastPageButtonClick}
                disabled={page >= Math.ceil(count / rowsPerPage) - 1}
            >
                <LastPage />
            </IconButton>
        </div>
    );
};

OmTablePaginationActions.defaultProps = {};

OmTablePaginationActions.propTypes = {
    classes: PropTypes.object.isRequired,
    count: PropTypes.number.isRequired,
    onChangePage: PropTypes.func.isRequired,
    page: PropTypes.number.isRequired,
    rowsPerPage: PropTypes.number.isRequired,
};

export default withStyles(OmTableStyle)(
    memo(OmTablePaginationActions),
);
