import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import withStyles from '@material-ui/core/styles/withStyles';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import { Tooltip } from '@material-ui/core';
import OmTableStyle from '../OmTableStyle';

const OmTableHeader = (props) => {
    const {
        headerGroups,
        classes,
        enableColumnFilter,
        headerAlign,
        handleOnSortChange,
        serverSide,
    } = props;
    const [currentSort, setCurrentSort] = React.useState('');
    const onSortClick = useCallback(
        (data) => {
            if (data.isSorted && serverSide) {
                const sort =
                    data.id + (data.isSortedDesc ? ',desc' : ',asc');
                // avoid duplicate request
                if (currentSort !== sort) {
                    setCurrentSort(sort);
                    handleOnSortChange(sort);
                }
            }
        },
        [serverSide, currentSort, setCurrentSort, handleOnSortChange],
    );

    const isActive = (column) => {
        if (serverSide && currentSort.length > 0) {
            const col = currentSort.split(',')[0];
            if (column.id === col) {
                return true;
            }
        }
        return column.isSorted;
    };

    const getSorted = useCallback(
        (column) => {
            if (serverSide && currentSort.length > 0) {
                const col = currentSort.split(',')[0];
                if (column.id === col) {
                    console.log(column, currentSort.split(',')[1]);
                    return currentSort.split(',')[1];
                }
            }
            return column.isSortedDesc ? 'desc' : 'asc';
        },
        [serverSide, currentSort],
    );

    return (
        <TableHead>
            {headerGroups.map((headerGroup) => (
                <TableRow {...headerGroup.getHeaderGroupProps()}>
                    {headerGroup.headers.map((column) => (
                        <Tooltip
                            title={column.Tooltip ?? ''}
                            key={column.id}
                        >
                            <TableCell
                                key={column.id}
                                align={headerAlign}
                                className={classes.tableHeadCell}
                                style={{
                                    width: column.width,
                                    minWidth: column.minWidth,
                                    maxWidth: column.maxWidth,
                                }}
                            >
                                <div
                                    style={{
                                        display: 'flex',
                                        flexDirection: 'column',
                                    }}
                                >
                                    {!column.disableSort && (
                                        <span>
                                            <TableSortLabel
                                                active={isActive(
                                                    column,
                                                )}
                                                direction={getSorted(
                                                    column,
                                                )}
                                                onClick={onSortClick(
                                                    column,
                                                )}
                                                {...column.getHeaderProps(
                                                    column.getSortByToggleProps(),
                                                )}
                                            >
                                                {column.render(
                                                    'Header',
                                                )}
                                            </TableSortLabel>
                                        </span>
                                    )}
                                    {enableColumnFilter &&
                                        column.canFilter &&
                                        column.render('Filter')}
                                </div>
                            </TableCell>
                        </Tooltip>
                    ))}
                </TableRow>
            ))}
        </TableHead>
    );
};

OmTableHeader.defaultProps = {
    serverSide: false,
    handleOnSortChange: undefined,
    headerAlign: 'right',
    denseView: false
};

OmTableHeader.propTypes = {
    classes: PropTypes.object.isRequired,
    headerGroups: PropTypes.array.isRequired,
    enableColumnFilter: PropTypes.bool.isRequired,
    serverSide: PropTypes.bool,
    handleOnSortChange: PropTypes.func,
    headerAlign: PropTypes.string,
    denseView: PropTypes.bool
};

export default withStyles(OmTableStyle)(OmTableHeader);
