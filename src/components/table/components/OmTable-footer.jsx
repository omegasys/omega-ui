import React from 'react';
import PropTypes from 'prop-types';
import TableFooter from '@material-ui/core/TableFooter';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import withStyles from '@material-ui/core/styles/withStyles';
import OmTableStyle from '../OmTableStyle';

const OmTableFooter = (props) => {
    const { classes, footerGroups, showFooter, footerAlign } = props;
    return (
        <TableFooter>
            {showFooter &&
                footerGroups.map((group) => (
                    <TableRow {...group.getFooterGroupProps()}>
                        {group.headers.map((column) => (
                            <TableCell
                                align={footerAlign}
                                key={column.id}
                                className={classes.tableFooterCell}
                                {...column.getFooterProps()}
                            >
                                {column.render('Footer')}
                            </TableCell>
                        ))}
                    </TableRow>
                ))}
        </TableFooter>
    );
};

OmTableFooter.defaultProps = { footerAlign: 'right' };

OmTableFooter.propTypes = {
    classes: PropTypes.object.isRequired,
    footerGroups: PropTypes.array.isRequired,
    showFooter: PropTypes.bool.isRequired,
    footerAlign: PropTypes.string,
};

export default withStyles(OmTableStyle)(OmTableFooter);
