import React, { useState } from 'react';
import PropTypes from 'prop-types';
import 'regenerator-runtime/runtime.js'; // eslint-disable-line import/extensions
import { useAsyncDebounce } from 'react-table';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import IconButton from '@material-ui/core/IconButton';
import { Add, Refresh, ViewColumn } from '@material-ui/icons';
import OmTableStyle from '../OmTableStyle';
import OmSearchField from '../../input/searchField/OmSearchField';
import OmPopover from '../../popover/OmPopover';
import useAnchorEl from '../../../hooks/useAnchorEl';
import OmTableViewColumn from './OmTable-viewColumn';

const OmTableToolbar = (props) => {
    const {
        title,
        globalFilter,
        setGlobalFilter,
        allColumns,
        handleAddAction,
        handleRefreshAction,
        classes,
        serverSide,
        handleOnFilterChange,
    } = props;
    const [searchFieldValue, setSearchFieldValue] = useState(
        globalFilter,
    );
    const searchFieldOnChange = useAsyncDebounce((value) => {
        setGlobalFilter(value || undefined);
    }, 200);
    const searchFieldOnChangeDirty = useAsyncDebounce((value) => {
        setGlobalFilter(value || undefined);
        handleOnFilterChange(searchFieldValue);
    }, 1000);
    const { anchorEl, onAnchorClick, onAnchorClose } = useAnchorEl();

    const renderToolbarTitle = () => (
        <Typography variant="h6" className={classes.toolbarTitle}>
            {title}
        </Typography>
    );

    const renderAddButton = () => (
        <IconButton onClick={handleAddAction}>
            <Add />
        </IconButton>
    );

    const renderRefreshButton = () => (
        <IconButton onClick={handleRefreshAction}>
            <Refresh />
        </IconButton>
    )

    const renderViewColumn = () => (
        <div>
            <IconButton onClick={onAnchorClick}>
                <ViewColumn />
            </IconButton>
            <OmPopover
                anchorEl={anchorEl}
                component={
                    <OmTableViewColumn allColumns={allColumns} />
                }
                handleClose={onAnchorClose}
            />
        </div>
    );

    return (
        <Toolbar>
            {renderToolbarTitle()}
            <OmSearchField
                value={searchFieldValue || ''}
                onChange={(e) => {
                    setSearchFieldValue(e.target.value);
                    if (serverSide) {
                        searchFieldOnChangeDirty(e.target.value);
                    } else {
                        searchFieldOnChange(e.target.value);
                    }
                }}
            />
            {handleAddAction && renderAddButton()}
            {handleRefreshAction && renderRefreshButton()}
            {renderViewColumn()}
        </Toolbar>
    );
};

OmTableToolbar.defaultProps = {
    title: '',
    allColumns: [],
    setGlobalFilter: undefined,
    handleAddAction: undefined,
    handleRefreshAction: undefined,
    serverSide: false,
    handleOnFilterChange: undefined,
};

OmTableToolbar.propTypes = {
    title: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
    globalFilter: PropTypes.string,
    setGlobalFilter: PropTypes.func,
    handleAddAction: PropTypes.func,
    handleRefreshAction: PropTypes.func,
    allColumns: PropTypes.array,
    classes: PropTypes.object.isRequired,
    serverSide: PropTypes.bool,
    handleOnFilterChange: PropTypes.func,
};

export default withStyles(OmTableStyle)(OmTableToolbar);
