import React from 'react';
import PropTypes from 'prop-types';
import Table from '@material-ui/core/Table';
import TableRow from '@material-ui/core/TableRow';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import OmTablePaginationActions from './OmTable-pagination-actions';

const OmTablePagination = (props) => {
    const {
        data,
        totalNumberOfRows,
        rowsPerPageOptions,
        page,
        rowsPerPage,
        handleChangePage,
        handleChangeRowsPerPage,
    } = props;

    return (
        <Table>
            <TableFooter>
                <TableRow>
                    <TablePagination
                        rowsPerPageOptions={rowsPerPageOptions}
                        count={totalNumberOfRows}
                        rowsPerPage={rowsPerPage}
                        page={page}
                        onChangePage={handleChangePage}
                        onChangeRowsPerPage={handleChangeRowsPerPage}
                        ActionsComponent={OmTablePaginationActions}
                    />
                </TableRow>
            </TableFooter>
        </Table>
    );
};

OmTablePagination.propTypes = {
    totalNumberOfRows: PropTypes.number.isRequired,
    rowsPerPageOptions: PropTypes.arrayOf(PropTypes.number)
        .isRequired,
    page: PropTypes.number.isRequired,
    rowsPerPage: PropTypes.number.isRequired,
    handleChangePage: PropTypes.func.isRequired,
    handleChangeRowsPerPage: PropTypes.func.isRequired,
};

export default OmTablePagination;
