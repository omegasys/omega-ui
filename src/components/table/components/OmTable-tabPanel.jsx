import React from 'react';
import PropTypes from 'prop-types';
import { TabPanel } from '@material-ui/lab';
import withStyles from '@material-ui/core/styles/withStyles';
import OmTableStyle from '../OmTableStyle';

const OmTableTabPanel = (props) => {
    const { value, children, classes } = props;

    return (
        <TabPanel value={value} className={classes.tableTabPanelRoot}>
            {children}
        </TabPanel>
    );
};

OmTableTabPanel.defaultProps = {};

OmTableTabPanel.propTypes = {
    value: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
        .isRequired,
    children: PropTypes.node.isRequired,
    classes: PropTypes.object.isRequired,
};

export default withStyles(OmTableStyle)(OmTableTabPanel);
