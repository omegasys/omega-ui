import React from 'react';
import PropTypes from 'prop-types';
import { Collapse } from '@material-ui/core';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import IconButton from '@material-ui/core/IconButton';
import makeStyles from '@material-ui/core/styles/makeStyles';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import { noop } from 'lodash';

const useRowStyles = makeStyles({
    root: {
        '& > *': {
            borderBottom: 'unset',
        },
    },
});

const OmTableRow = (props) => {
    const { row, classes, rowAlign, onExpand, denseView } = props;
    const [open, setOpen] = React.useState(false);
    const [subRowColSpan, setSubRowColSpan] = React.useState(12);

    const isJSON = (value) => {
        try {
            JSON.parse(value);
        } catch (error) {
            return false;
        }
        return true;
    };

    const isJsonColumn = (cell) =>
        cell.column.format &&
        cell.column.format === 'json' &&
        isJSON(cell.value);

    const processCell = (cell) => {
        if (isJsonColumn(cell) || cell.column.isExpandable) {
            return (
                <IconButton
                    aria-label="expand row"
                    size="small"
                    onClick={() => {
                        !open && onExpand(cell);
                        setOpen(!open);
                    }}
                >
                    {open ? (
                        <KeyboardArrowUpIcon />
                    ) : (
                        <KeyboardArrowDownIcon />
                    )}
                </IconButton>
            );
        }
        return cell.render('Cell');
    };

    const processExpandedRow = (cell) => {
        if (cell.column.subRow) {
            const subRow = cell.column.subRow(cell);
            if (subRowColSpan <= 12 && subRow?.props?.cols?.length) {
                setSubRowColSpan(subRow.props.cols.length);
            }

            return subRow;
        }
        return '';
    };

    return (
        <React.Fragment key={row.id + 'frag'}>
            <TableRow
                className={classes.tableBodyRow}
                key={row.id}
                {...row.getRowProps()}
            >
                {row.cells.map((cell, i) => (
                    <TableCell
                        key={'cell' + row.id + '-' + i}
                        align={rowAlign}
                        className={denseView ? classes.tableCellDense : ''}
                        {...cell.getCellProps()}
                    >
                        {processCell(cell)}
                    </TableCell>
                ))}
            </TableRow>
            <TableRow key={row.id + 'collapse'}>
                <TableCell
                    className={denseView ? classes.tableCellDense : ''}
                    style={{
                        borderBottom: 0,
                        paddingBottom: 0,
                        paddingTop: 0,
                        paddingRight: 0,
                    }}
                    colSpan={subRowColSpan}
                >
                    <Collapse in={open} timeout="auto" unmountOnExit>
                        {row.cells.map((cell, i) => (
                            <React.Fragment key={i + 'subrow'}>
                                {processExpandedRow(cell)}
                            </React.Fragment>
                        ))}
                    </Collapse>
                </TableCell>
            </TableRow>
        </React.Fragment>
    );
};

OmTableRow.defaultProps = {
    row: undefined,
    rowAlign: 'right',
    onExpand: () => noop(),
    denseView: false
};

OmTableRow.propTypes = {
    row: PropTypes.object,
    classes: PropTypes.object.isRequired,
    rowAlign: PropTypes.string,
    onExpand: PropTypes.func,
    denseView: PropTypes.bool
};

export default OmTableRow;
