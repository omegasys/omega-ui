import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Tab from '@material-ui/core/Tab';
import { TabList } from '@material-ui/lab';

const OmTableTab = (props) => {
    const { value, tabList, onChange } = props;
    return (
        <AppBar position="static" color="transparent">
            <TabList
                value={value}
                onChange={onChange}
                indicatorColor="primary"
                aria-label="om-table-tab"
            >
                {tabList.map((tab, index) => (
                    <Tab
                        label={tab.label}
                        value={tab.accessor}
                        key={index}
                    />
                ))}
            </TabList>
        </AppBar>
    );
};

OmTableTab.defaultProps = {
    tabList: [],
};

OmTableTab.propTypes = {
    value: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
        .isRequired,
    tabList: PropTypes.arrayOf(
        PropTypes.shape({
            accessor: PropTypes.string,
            label: PropTypes.oneOfType([
                PropTypes.string,
                PropTypes.element,
            ]),
        }),
    ),
    onChange: PropTypes.func.isRequired,
};

export default OmTableTab;
