import { TableBody, TableCell, Typography } from '@material-ui/core';
import withStyles from '@material-ui/core/styles/withStyles';
import TableRow from '@material-ui/core/TableRow';
import { Info } from '@material-ui/icons';
import { isEmpty, map } from 'lodash';
import PropTypes from 'prop-types';
import React, { memo } from 'react';
import { generateRandomKey } from '../../../utils/OmUtils';
import OmLoadingOverlay from '../../loading/OmLoadingOverlay';
import OmTableStyle from '../OmTableStyle';
import OmTableRow from './OmTable-row';
import OmTableSubRow from "./OmTable-subRow";

const OmTableBody = (props) => {
    const {
        classes,
        page,
        prepareRow,
        getTableBodyProps,
        rowAlign,
        loading,
        onExpand,
        denseView
    } = props;

    return (
        <TableBody {...getTableBodyProps()}>
            {loading ? (
                <TableRow>
                    <TableCell
                        style={{
                            paddingTop: 20,
                            paddingBottom: 20,
                            borderBottom: 0,
                        }}
                        colSpan={12}
                    >
                        <OmLoadingOverlay />
                    </TableCell>
                </TableRow>
            ) : isEmpty(page) ? (
                <TableRow>
                    <TableCell
                        style={{
                            paddingTop: 20,
                            paddingBottom: 20,
                            borderBottom: 0,
                        }}
                        colSpan={12}
                    >
                        <div
                            style={{
                                height: '100%',
                                display: 'flex',
                                flexDirection: 'column',
                                alignItems: 'center',
                                justifyContent: 'center',
                                margin: '1rem 0',
                            }}
                        >
                            <Info
                                color="primary"
                                style={{ fontSize: '4rem' }}
                            />
                            <Typography component="h5">
                                No Data
                            </Typography>
                        </div>
                    </TableCell>
                </TableRow>
            ) : (
                map(page, (row, i) => {
                    prepareRow(row);
                    return (
                        // Use a React.Fragment here so the table markup is still valid
                        <React.Fragment key={generateRandomKey()}>
                            <OmTableRow
                                row={row}
                                classes={classes}
                                rowAlign={rowAlign}
                                onExpand={onExpand}
                                denseView={denseView}
                            />
                            {/* We could pass anything into this */}
                            {   row.isExpanded &&
                                <OmTableSubRow
                                    row={row}
                                    rowAlign={rowAlign}
                                    classes={classes}
                                    data={row.original.children}
                                    denseView={denseView}
                                    {...row.getRowProps()}
                                />}
                        </React.Fragment>
                    );
                })
            )}
        </TableBody>
    );
};

OmTableBody.defaultProps = {
    rowAlign: 'right',
    loading: false,
    denseView: false
};

OmTableBody.propTypes = {
    classes: PropTypes.object.isRequired,
    page: PropTypes.array.isRequired,
    prepareRow: PropTypes.func.isRequired,
    getTableBodyProps: PropTypes.func.isRequired,
    rowAlign: PropTypes.string,
    loading: PropTypes.bool,
    onExpand: PropTypes.func,
    denseView: PropTypes.bool
};

export default withStyles(OmTableStyle)(memo(OmTableBody));
