import React from 'react';
import PropTypes from 'prop-types';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import { FormLabel } from '@material-ui/core';
import FormControl from '@material-ui/core/FormControl';

const OmTableViewColumn = (props) => {
    const { allColumns } = props;
    return (
        <FormControl>
            <FormLabel component="legend">Show Columns</FormLabel>
            <FormGroup>
                {allColumns.map((column) => (
                    <FormControlLabel
                        key={column.id}
                        control={
                            <Checkbox
                                color="primary"
                                {...column.getToggleHiddenProps()}
                            />
                        }
                        label={column.Header}
                    />
                ))}
            </FormGroup>
        </FormControl>
    );
};

OmTableViewColumn.defaultProps = {
    allColumns: [],
};

OmTableViewColumn.propTypes = {
    allColumns: PropTypes.array,
};

export default OmTableViewColumn;
