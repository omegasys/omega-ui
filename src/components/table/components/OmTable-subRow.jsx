import React from 'react';
import PropTypes from 'prop-types';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';

const OmTableSubRow = (props) => {
    const { row, classes, rowAlign, data, denseView } = props;

    return (
        <>
            {data.map((x, i) => {
                return (
                    <TableRow
                        className={classes.tableBodySubRow}
                        {...row.getRowProps()}
                        key={`${row.id}-expanded-${i}`}
                    >
                        {row.cells.map((cell) => {
                            return (
                                <TableCell
                                    className={denseView ? classes.tableCellDense : ''}
                                    align={rowAlign}
                                    {...cell.getCellProps()}
                                >
                                    {cell.render(cell.column.SubCell ? 'SubCell' : 'Cell', {
                                        value:
                                            cell.column.accessor &&
                                            cell.column.accessor(x, i),
                                        row: { ...row, original: x }
                                    })}
                                </TableCell>
                            );
                        })}
                    </TableRow>
                );
            })}
        </>
    );
};

OmTableSubRow.defaultProps = {
    row: undefined,
    data: [],
    rowAlign: 'right',
    denseView: false
};

OmTableSubRow.propTypes = {
    row: PropTypes.object,
    data: PropTypes.arrayOf(PropTypes.object),
    classes: PropTypes.object.isRequired,
    rowAlign: PropTypes.string,
    denseView: PropTypes.bool
};

export default OmTableSubRow;
