import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import { Edit } from '@material-ui/icons';
import OmDataTable from './OmDataTable';

export default {
    title: 'Table/Data Table',
    component: OmDataTable,
};

const Template = (args) => <OmDataTable {...args} />;

const createData = (dessert, calories, fat, carbs, protein) => ({
    dessert,
    calories,
    fat,
    carbs,
    protein,
});

export const Default = Template.bind({});
Default.args = {
    title: 'Dessert Table',
    columns: [
        {
            accessor: 'action',
            Header: 'Actions',
            // eslint-disable-next-line react/prop-types
            Cell: ({ row }) => (
                <IconButton
                    onClick={() => {
                        console.log(row);
                    }}
                >
                    <Edit />
                </IconButton>
            ),
            disableSortBy: true,
        },
        { accessor: 'dessert', Header: 'Dessert(100g)' },
        { accessor: 'calories', Header: 'Calories' },
        { accessor: 'fat', Header: 'Fat(g)' },
        { accessor: 'carbs', Header: 'Carbs(g)' },
        { accessor: 'protein', Header: 'Protein(g)' },
    ],
    data: [
        createData('Frozen Yoghurt', 159, 6, 24, 4),
        createData('Ice Cream Sandwich', 237, 9, 37, 4.3),
        createData('Eclair', 262, 16, 24, 6),
        createData('Cupcake', 305, 3.7, 67, 4.3),
        createData('Gingerbread', 356, 16.0, 5, 5),
        createData('Honeycomb', 408, 3.2, 5, 5),
        createData('Jelly Bean', 375, 0.0, 5, 5),
        createData('KitKat', 518, 26.0, 5, 5),
        createData('Lollipop', 392, 0.2, 5, 5),
        createData('Marshmallow', 318, 0, 5, 5),
        createData('Nougat', 360, 19.0, 5, 5),
        createData('Oreo', 437, 18.0, 5, 5),
    ],
};

export const WithFooterData = Template.bind({});
WithFooterData.args = {
    ...Default.args,
    title: 'Dessert Table With Footer Data',
    columns: [
        {
            accessor: 'action',
            Header: 'Actions',
            // eslint-disable-next-line react/prop-types
            Cell: ({ row }) => (
                <IconButton
                    onClick={() => {
                        console.log(row);
                    }}
                >
                    <Edit />
                </IconButton>
            ),
            disableSortBy: true,
        },
        {
            accessor: 'dessert',
            Header: 'Dessert(100g)',
            Footer: 'Totals',
        },
        {
            accessor: 'calories',
            Header: 'Calories',
            Footer: (data) => {
                const total = React.useMemo(
                    () =>
                        data.rows.reduce(
                            (sum, row) => row.values.calories + sum,
                            0,
                        ),
                    [data.rows],
                );
                return <>{total}</>;
            },
        },
        {
            accessor: 'fat',
            Header: 'Fat(g)',
            Footer: (data) => {
                const total = React.useMemo(
                    () =>
                        data.rows.reduce(
                            (sum, row) => row.values.fat + sum,
                            0,
                        ),
                    [data.rows],
                );
                return <>{total}</>;
            },
        },
        {
            accessor: 'carbs',
            Header: 'Carbs(g)',
            Footer: (data) => {
                const total = React.useMemo(
                    () =>
                        data.rows.reduce(
                            (sum, row) => row.values.carbs + sum,
                            0,
                        ),
                    [data.rows],
                );
                return <>{total}</>;
            },
        },
        {
            accessor: 'protein',
            Header: 'Protein(g)',
            Footer: (data) => {
                const total = React.useMemo(
                    () =>
                        data.rows.reduce(
                            (sum, row) => row.values.protein + sum,
                            0,
                        ),
                    [data.rows],
                );
                return <>{total}</>;
            },
        },
    ],
    showFooter: true,
    showPagination: false,
};

export const MultiTabTable = Template.bind({});
MultiTabTable.args = {
    title: 'Multi Currency Table',
    multiTabTable: true,
    multiTabTableList: [
        { accessor: 'usd', label: 'USD' },
        { accessor: 'cad', label: 'CAD' },
        { accessor: 'eur', label: 'EUR' },
    ],
    columns: {
        usd: [
            {
                accessor: 'headerUsd',
                Header: 'USD Header',
            },
        ],
        cad: [
            {
                accessor: 'headerCad',
                Header: 'CAD Header',
            },
        ],
        eur: [
            {
                accessor: 'headerEur',
                Header: 'EUR Header',
            },
        ],
    },
    data: {
        usd: [{ headerUsd: 'USD' }],
        cad: [{ headerCad: 'CAD' }],
        eur: [{ headerEur: 'EUR' }],
    },
};
