import React, { useState } from 'react';
import PropTypes from 'prop-types';
import OmSearchField from '../../input/searchField/OmSearchField';
import { useAsyncDebounce } from 'react-table';

const OmTableServerSideColumnFilter = (props) => {
    const { column, customFilter, dirty } = props;
    const [searchValue, setSearchValue] = useState(
        dirty[column.id] || '',
    );

    const searchFieldOnChange = useAsyncDebounce((value) => {
        customFilter(column.id, value || undefined);
    }, 1000);

    return (
        <OmSearchField
            value={searchValue}
            onChange={(e) => {
                setSearchValue(e.target.value || '');
                searchFieldOnChange(e.target.value);
            }}
        />
    );
};

OmTableServerSideColumnFilter.propTypes = {
    column: PropTypes.object.isRequired,
    customFilter: PropTypes.func.isRequired,
    dirty: PropTypes.object.isRequired,
};

export default OmTableServerSideColumnFilter;
