import React from 'react';
import PropTypes from 'prop-types';
import OmSearchField from '../../input/searchField/OmSearchField';

const OmTableDefaultColumnFilter = (props) => {
    const {
        column: { filterValue, setFilter },
    } = props;

    return (
        <OmSearchField
            value={filterValue || ''}
            onChange={(e) => {
                setFilter(e.target.value || undefined);
            }}
        />
    );
};

OmTableDefaultColumnFilter.propTypes = {
    column: PropTypes.object.isRequired,
};

export default OmTableDefaultColumnFilter;
