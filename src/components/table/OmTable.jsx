import React from 'react';
import PropTypes from 'prop-types';
import OmDataTable from './OmDataTable';

/**
 * OmTable is a simple table without any table features.
 * This component is a wrapper of OmDataTable
 */
const OmTable = (props) => {
    const {
        columns,
        data,
        size,
        showPagination,
        showTableToolbar,
        serverSide,
        handleDirtyAction,
        ...rest
    } = props;
    return (
        <OmDataTable
            columns={columns}
            data={data}
            size={size}
            serverSide={serverSide}
            handleDirtyAction={handleDirtyAction}
            showPagination={showPagination}
            showTableToolbar={showTableToolbar}
            {...rest}
        />
    );
};

OmTable.defaultProps = {
    columns: [],
    data: [],
    size: 'small',
    showPagination: false,
    showTableToolbar: false,
    serverSide: false,
    handleDirtyAction: undefined,
};

OmTable.propTypes = {
    columns: PropTypes.array,
    data: PropTypes.arrayOf(PropTypes.object),
    size: PropTypes.oneOf(['small', 'medium']),
    showPagination: PropTypes.bool,
    showTableToolbar: PropTypes.bool,
    serverSide: PropTypes.bool,
    handleDirtyAction: PropTypes.func,
};

export default OmTable;
