import Table from '@material-ui/core/Table';
import TableContainer from '@material-ui/core/TableContainer';
import { TabContext } from '@material-ui/lab';
import PropTypes from 'prop-types';
import React, { useEffect, useMemo, useState } from 'react';
import {
    useFilters,
    useGlobalFilter,
    usePagination,
    useSortBy,
    useTable,
    useExpanded
} from 'react-table';
import OmTableBody from './components/OmTable-body';
import OmTableFooter from './components/OmTable-footer';
import OmTableHeader from './components/OmTable-header';
import OmTablePagination from './components/OmTable-pagination';
import OmTableTab from './components/OmTable-tab';
import OmTableTabPanel from './components/OmTable-tabPanel';
import OmTableToolbar from './components/OmTable-toolbar';
import OmTableDefaultColumnFilter from './customFilters/OmTable-defaultColumnFilter';
import OmTableServerSideColumnFilter from './customFilters/OmTable-serverSideColumnFilter';

/**
 * OmDataTable provides powerful features for tables and is designed for use cases that are focused around handling a large amounts of tabular data.
 * Please refer to https://react-table.tanstack.com/ V7 for features and options
 */
export const OmDataTable = (props) => {
    const {
        title,
        data,
        columns,
        handleAddAction,
        handleRefreshAction,
        size,
        showPagination,
        showTableToolbar,
        rowsPerPageOptions,
        showFooter,
        enableColumnFilter,
        stickyHeader,
        hiddenColumns,
        sortBy,
        multiTabTable,
        multiTabTableList,
        multiTabOnChange,
        serverSide,
        loading,
        handleDirtyAction,
        pageData,
        headerAlign,
        rowAlign,
        footerAlign,
        onExpand,
        denseView
    } = props;
    const multiTabTableListProps = useMemo(() => multiTabTableList, [
        multiTabTableList,
    ]);
    const dirty = {
        page: pageData.number,
        size: pageData.size,
        filter: null,
        sort: null,
    };
    const [tabValue, setTabValue] = useState(
        multiTabTableListProps.length > 0 &&
            multiTabTableListProps[0].accessor,
    );
    const columnsProps = useMemo(
        () =>
            multiTabTable
                ? columns[tabValue]
                    ? columns[tabValue]
                    : []
                : columns,
        [columns, multiTabTable, tabValue],
    );
    const dataProps = useMemo(
        () =>
            multiTabTable
                ? data[tabValue]
                    ? data[tabValue]
                    : []
                : data,
        [data, multiTabTable, tabValue],
    );
    const hiddenColumnsProps = useMemo(() => hiddenColumns, [
        hiddenColumns,
    ]);
    const sortByProps = useMemo(() => sortBy, [sortBy]);
    const defaultColumn = useMemo(
        () => ({
            Filter: (instance) =>
                !serverSide ? (
                    <OmTableDefaultColumnFilter {...instance} />
                ) : (
                    <OmTableServerSideColumnFilter
                        {...instance}
                        customFilter={handleOnColumnFilterChange}
                        dirty={dirty}
                    />
                ),
        }),
        [],
    );

    useEffect(() => {
        setPageSize(
            showPagination
                ? serverSide
                    ? pageData.size
                    : rowsPerPageOptions[0]
                : dataProps.length,
        );
    }, [
        dataProps,
        showPagination,
        serverSide,
        pageData,
        rowsPerPageOptions,
    ]);

    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        footerGroups,
        page,
        prepareRow,
        gotoPage,
        setPageSize,
        setGlobalFilter,
        allColumns,
        state: { pageIndex, pageSize, globalFilter },
    } = useTable(
        {
            columns: columnsProps,
            data: dataProps,
            defaultColumn,
            initialState: {
                pageSize: showPagination
                    ? serverSide
                        ? pageData.size
                        : rowsPerPageOptions[0]
                    : dataProps.length,
                hiddenColumns: hiddenColumnsProps,
                sortBy: sortByProps,
            },
            ...(!showPagination
                ? { manualPagination: true, pageCount: -1 }
                : {}),
        },
        useFilters,
        useGlobalFilter,
        useSortBy,
        useExpanded,
        usePagination
    );

    const handleChangePage = (value) => {
        if (serverSide) {
            dirty.page = value;
            handleDirtyAction(dirty);
        } else {
            gotoPage(value);
        }
    };

    const handleChangeRowsPerPage = (event) => {
        setPageSize(parseInt(event.target.value, 10));
        if (serverSide) {
            dirty.page = 0;
            dirty.size = event.target.value;
            handleDirtyAction(dirty);
        } else {
            gotoPage(0);
        }
    };

    const handleOnSortChange = (sort) => {
        if (serverSide) {
            dirty.sort = sort;
            handleDirtyAction(dirty);
        }
    };

    const handleOnFilterChange = (value) => {
        dirty.filter = value;
        if (serverSide) {
            dirty.page = 0;
        }
        handleDirtyAction(dirty);
    };

    const handleOnColumnFilterChange = (columnId, value) => {
        dirty[columnId] = value;
        handleDirtyAction(dirty);
    };

    const handleTabChange = (event, newValue) => {
        if (multiTabOnChange) {
            multiTabOnChange(newValue);
        }
        setTabValue(newValue);
    };

    const renderTable = () => (
        <>
            <TableContainer>
                <Table
                    size={size}
                    stickyHeader={stickyHeader}
                    {...getTableProps()}
                >
                    <OmTableHeader
                        headerGroups={headerGroups}
                        enableColumnFilter={enableColumnFilter}
                        headerAlign={headerAlign}
                        handleOnSortChange={handleOnSortChange}
                        serverSide={serverSide}
                        denseView={denseView}
                    />
                    <OmTableBody
                        getTableBodyProps={getTableBodyProps}
                        page={page}
                        prepareRow={prepareRow}
                        rowAlign={rowAlign}
                        loading={loading}
                        onExpand={onExpand}
                        denseView={denseView}
                    />
                    <OmTableFooter
                        footerGroups={footerGroups}
                        showFooter={showFooter}
                        footerAlign={footerAlign}
                    />
                </Table>
            </TableContainer>
            {showPagination && (
                <OmTablePagination
                    totalNumberOfRows={
                        serverSide
                            ? pageData.totalElements
                            : dataProps.length
                    }
                    page={serverSide ? pageData.number : pageIndex}
                    rowsPerPage={
                        serverSide ? pageData.size : pageSize
                    }
                    rowsPerPageOptions={rowsPerPageOptions}
                    handleChangePage={handleChangePage}
                    handleChangeRowsPerPage={handleChangeRowsPerPage}
                />
            )}
        </>
    );

    const renderTableInTab = () => (
        <TabContext value={tabValue}>
            <OmTableTab
                value={tabValue}
                tabList={multiTabTableListProps}
                onChange={handleTabChange}
            />
            {multiTabTableList.map((tab, index) => (
                <OmTableTabPanel value={tab.accessor} key={index}>
                    {renderTable()}
                </OmTableTabPanel>
            ))}
        </TabContext>
    );

    return (
        <div>
            {showTableToolbar && (
                <OmTableToolbar
                    title={title}
                    globalFilter={globalFilter}
                    setGlobalFilter={setGlobalFilter}
                    handleAddAction={handleAddAction}
                    handleRefreshAction={handleRefreshAction}
                    serverSide={serverSide}
                    handleOnFilterChange={handleOnFilterChange}
                    allColumns={allColumns}
                />
            )}
            {multiTabTable ? renderTableInTab() : renderTable()}
        </div>
    );
};

OmDataTable.defaultProps = {
    title: undefined,
    columns: [],
    data: [],
    size: 'small',
    handleAddAction: undefined,
    handleRefreshAction: undefined,
    showTableToolbar: true,
    showFooter: false,
    showPagination: true,
    rowsPerPageOptions: [5, 10, 20],
    enableColumnFilter: false,
    sortBy: [],
    serverSide: false,
    loading: false,
    handleDirtyAction: undefined,
    pageData: {
        size: 0,
        number: 0,
        totalElements: 0,
        totalPages: 0,
    },
    headerAlign: 'right',
    rowAlign: 'right',
    footerAlign: 'right',
    hiddenColumns: [],
    stickyHeader: true,
    multiTabTable: false,
    multiTabTableList: [],
    multiTabOnChange: undefined,
    denseView: false
};

OmDataTable.propTypes = {
    title: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
    columns: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
    data: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.object),
        PropTypes.object,
    ]),
    size: PropTypes.oneOf(['small', 'medium']),
    handleAddAction: PropTypes.func,
    handleRefreshAction: PropTypes.func,
    showTableToolbar: PropTypes.bool,
    showFooter: PropTypes.bool,
    showPagination: PropTypes.bool,
    rowsPerPageOptions: PropTypes.arrayOf(PropTypes.number),
    enableColumnFilter: PropTypes.bool,
    hiddenColumns: PropTypes.array,
    serverSide: PropTypes.bool,
    loading: PropTypes.bool,
    handleDirtyAction: PropTypes.func,
    pageData: PropTypes.shape({
        size: PropTypes.number,
        number: PropTypes.number,
        totalElements: PropTypes.number,
        totalPages: PropTypes.number,
    }),
    headerAlign: PropTypes.string,
    rowAlign: PropTypes.string,
    footerAlign: PropTypes.string,
    sortBy: PropTypes.array,
    stickyHeader: PropTypes.bool,
    multiTabTable: PropTypes.bool,
    multiTabTableList: PropTypes.arrayOf(
        PropTypes.shape({
            accessor: PropTypes.string,
            label: PropTypes.oneOfType([
                PropTypes.string,
                PropTypes.element,
            ]),
        }),
    ),
    multiTabOnChange: PropTypes.func,
    onExpand: PropTypes.func,
    denseView: PropTypes.bool
};

export default OmDataTable;
