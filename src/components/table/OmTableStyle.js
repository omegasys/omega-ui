const OmTableStyle = (theme) => ({
    tableHeadCell: {
        fontWeight: 'bold',
    },
    tableBodyRow: {
        '&:nth-of-type(even)': {
            backgroundColor: '#fafafa',
        },
    },
    tableBodySubRow: {
        backgroundColor: '#f3f3f3',
    },
    tableCellDense: {
        whiteSpace: 'nowrap'
    },
    tableFooterCell: {
        color: 'black',
    },
    tablePaginationActionsRoot: {
        flexShrink: 0,
        marginLeft: theme.spacing(2.5),
    },
    toolbarTitle: {
        flexGrow: 1,
        display: 'none',
        [theme.breakpoints.up('sm')]: {
            display: 'block',
        },
    },
    tableTabPanelRoot: {
        padding: 0,
    },
});

export default OmTableStyle;
