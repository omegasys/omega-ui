import React from 'react';
import OmTable from './OmTable';
import * as OmDataTableStories from './OmDataTable.stories';

export default {
    title: 'Table/Table',
    component: OmTable,
};

const Template = (args) => <OmTable {...args} />;

const createData = (id, firstName, lastName, age) => ({
    id,
    firstName,
    lastName,
    age,
});

export const Default = Template.bind({});
Default.args = {
    columns: [
        { accessor: 'id', Header: 'ID' },
        { accessor: 'firstName', Header: 'First name' },
        { accessor: 'lastName', Header: 'Last name' },
        { accessor: 'age', Header: 'Age' },
    ],
    data: [
        createData('1', 'Harvey', 'Roxie', 24),
        createData('2', 'Rossini', 'Frances', 37),
        createData('3', 'Ferrara', 'Clifford', 24),
        createData('4', 'Daenerys', 'Melisandre', 67),
    ],
};

export const MultiTabTable = Template.bind({});
MultiTabTable.args = {
    ...OmDataTableStories.MultiTabTable.args,
};
