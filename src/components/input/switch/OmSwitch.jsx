import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import PropTypes from 'prop-types';
import React, { memo } from 'react';

export function OmSwitch(props) {
    const { label, labelPlacement, className, ...others } = props;
    return (
        <FormControlLabel
            control={
                <div className={className}>
                    <Switch {...others} />
                </div>
            }
            label={label}
            labelPlacement={labelPlacement}
        />
    );
}

OmSwitch.propTypes = {
    id: PropTypes.string,
    className: PropTypes.string,
    label: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.node,
        PropTypes.object,
    ]),
    checked: PropTypes.bool,
    color: PropTypes.oneOf(['primary', 'secondary', 'default']),
    size: PropTypes.oneOf(['small', 'medium']),
    onChange: PropTypes.func,
};

OmSwitch.defaultProps = {
    color: 'primary',
    size: 'medium',
};

export default memo(OmSwitch);
