import React from 'react';
import OmSwitch from './OmSwitch';

export default {
    title: 'Input/Switch',
    component: OmSwitch,
    argTypes: {
        onChange: { action: 'clicked' },
    },
};

const Template = (args) => <OmSwitch {...args} />;

export const Default = Template.bind({});

Default.args = {
    label: 'Label',
};
