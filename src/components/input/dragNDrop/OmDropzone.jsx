import React from 'react';
import PropTypes from 'prop-types';
import Dropzone from 'react-dropzone';
import withStyles from '@material-ui/core/styles/withStyles';
import { CloudUpload } from '@material-ui/icons';
import OmDropzoneStyles from './OmDropzoneStyles';

/**
 * Refer to https://react-dropzone.js.org/ for documentation, basic and advanced usage
 * @param props
 * @return {*}
 * @constructor
 */
export const OmDropzone = (props) => {
    const { classes, label, ...rest } = props;

    return (
        <Dropzone {...rest}>
            {({ getRootProps, getInputProps }) => (
                <section>
                    <div
                        {...getRootProps({
                            className: classes.baseStyle,
                        })}
                    >
                        <input {...getInputProps()} />
                        <CloudUpload fontSize="large" />
                        <p style={{ marginLeft: '5px' }}>{label}</p>
                    </div>
                </section>
            )}
        </Dropzone>
    );
};

OmDropzone.defaultProps = {
    label: 'Drag and drop to upload',
};

OmDropzone.propTypes = {
    classes: PropTypes.object.isRequired,
    label: PropTypes.string,
};

export default withStyles(OmDropzoneStyles)(OmDropzone);
