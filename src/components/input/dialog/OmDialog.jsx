import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import PropTypes from 'prop-types';
import React, { memo } from 'react';
import OmButton from '../../button/OmButton';

export function OmDialog(props) {
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
    const { open, maxWidth, children, handleClose, ...rest } = props;
    return (
        <Dialog
            fullScreen={fullScreen}
            maxWidth={maxWidth}
            open={open}
            {...rest}
        >
            <DialogContent>
                <DialogContentText>
                    Let Google help apps determine location. This
                    means sending anonymous location data to Google,
                    even when no apps are running.
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <OmButton
                    autoFocus
                    onClick={handleClose}
                    color="primary"
                >
                    Disagree
                </OmButton>
                <OmButton
                    onClick={handleClose}
                    color="primary"
                    autoFocus
                >
                    Agree
                </OmButton>
            </DialogActions>
        </Dialog>
    );
}

OmDialog.propTypes = {
    open: PropTypes.bool,
    children: PropTypes.node,
    handleClose: PropTypes.func,
    maxWidth: PropTypes.oneOf(['xs', 'sm', 'md', 'lg', 'xl']),
};

OmDialog.defaultProps = {
    maxWidth: 'xs',
};

export default memo(OmDialog);
