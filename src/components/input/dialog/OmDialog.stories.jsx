import React from 'react';
import OmDialog from './OmDialog';

export default {
    title: 'Input/Dialog',
    component: OmDialog,
    argTypes: {
        onChange: { action: 'clicked' },
    },
};

const Template = (args) => <OmDialog {...args} />;

export const Default = Template.bind({});

Default.args = {
    open: true,
    maxWidth: 'lg',
};
