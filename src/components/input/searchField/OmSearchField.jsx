import React from 'react';
import PropTypes from 'prop-types';
import { Search } from '@material-ui/icons';
import TextField from '@material-ui/core/TextField';
import { InputAdornment } from '@material-ui/core';

const OmSearchField = (props) => {
    const { placeholder, ...rest } = props;
    return (
        <TextField
            placeholder={placeholder}
            InputProps={{
                startAdornment: (
                    <InputAdornment position="start">
                        <Search />
                    </InputAdornment>
                ),
            }}
            color="primary"
            {...rest}
        />
    );
};

OmSearchField.defaultProps = {
    placeholder: 'Search...',
};

OmSearchField.propTypes = {
    placeholder: PropTypes.string,
};

export default OmSearchField;
