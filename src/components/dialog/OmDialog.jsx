import DialogActions from '@material-ui/core/DialogActions';
import PropTypes from 'prop-types';
import React, { memo } from 'react';
import { Dialog, DialogContent } from '@material-ui/core';
import withStyles from '@material-ui/core/styles/withStyles';
import OmButton from '../button/OmButton';
import OmDialogHeader from './OmDialog-header';
import OmDialogPaperComponent from './OmDialog-paper';
import OmDialogStyles from './OmDialogStyles';

const OmDialog = (props) => {
    const {
        classes,
        open,
        size,
        initialHeight,
        initialWidth,
        children,
        title,
        onClose,
        onCancel,
        onConfirm,
        confirmLabel,
        cancelLabel,
        confirmDisabled,
        cancelDisabled,
        hasCancel,
        hasFooter,
        ...rest
    } = props;

    const renderDialogActions = () => {
        if (hasFooter) {
            return (
                <DialogActions>
                    <OmButton
                        variant="text"
                        color="primary"
                        onClick={onConfirm}
                        size="small"
                        disabled={confirmDisabled}
                    >
                        {confirmLabel}
                    </OmButton>
                    {hasCancel && (
                        <OmButton
                            variant="text"
                            color="default"
                            onClick={onCancel}
                            size="small"
                            disabled={cancelDisabled}
                        >
                            {cancelLabel}
                        </OmButton>
                    )}
                </DialogActions>
            );
        }
        return null;
    };

    return (
        <Dialog
            onClose={onClose}
            open={open}
            PaperProps={{
                component: OmDialogPaperComponent,
                size,
                initialHeight,
                initialWidth
            }}
            hideBackdrop={true}
            disableBackdropClick={true}
            maxWidth={'xl'}
            aria-labelledby="draggable-dialog-title"
            {...rest}
        >
            <>
                {title && (
                    <OmDialogHeader onClose={onClose}>
                        {title}
                    </OmDialogHeader>
                )}
                <DialogContent>{children}</DialogContent>
                {renderDialogActions()}
            </>
        </Dialog>
    );
};

OmDialog.propTypes = {
    classes: PropTypes.object,
    open: PropTypes.bool.isRequired,
    title: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    onClose: PropTypes.func,
    children: PropTypes.node,
    onCancel: PropTypes.func,
    onConfirm: PropTypes.func,
    confirmLabel: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.object,
    ]),
    cancelLabel: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.object,
    ]),
    confirmDisabled: PropTypes.bool,
    cancelDisabled: PropTypes.bool,
    hasCancel: PropTypes.bool,
    hasFooter: PropTypes.bool,
    size: PropTypes.oneOf(['xs', 'sm', 'md', 'lg', 'xl']),
    initialHeight: PropTypes.number,
    initialWidth: PropTypes.number
};

OmDialog.defaultProps = {
    classes: undefined,
    title: undefined,
    onClose: undefined,
    children: undefined,
    onCancel: undefined,
    onConfirm: undefined,
    confirmLabel: 'Confirm',
    cancelLabel: 'Cancel',
    confirmDisabled: false,
    cancelDisabled: false,
    hasCancel: true,
    hasFooter: true,
    size: 'md',
};

export default withStyles(OmDialogStyles)(memo(OmDialog));
