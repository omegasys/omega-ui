import React, { memo } from 'react';
import PropTypes from 'prop-types';
import DialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import { Close } from '@material-ui/icons';
import withStyles from '@material-ui/core/styles/withStyles';
import OmDialogStyles from './OmDialogStyles';

const OmDialogHeader = (props) => {
    const { children, onClose, classes } = props;
    return (
        <DialogTitle id="draggable-dialog-title">
            {children}
            {onClose && (
                <IconButton
                    onClick={onClose}
                    className={classes.closeButton}
                >
                    <Close />
                </IconButton>
            )}
        </DialogTitle>
    );
};

OmDialogHeader.defaultProps = {
    onClose: undefined,
    children: undefined,
};

OmDialogHeader.propTypes = {
    classes: PropTypes.object.isRequired,
    children: PropTypes.node,
    onClose: PropTypes.func,
};

export default withStyles(OmDialogStyles)(memo(OmDialogHeader));
