const OmDialogStyles = (theme) => ({
    closeButton: {
        position: 'absolute',
        right: theme.spacing(1),
        top: theme.spacing(1),
        color: theme.palette.grey[500],
    },
    resizable: {
        position: 'relative',
        '& .react-resizable-handle': {
            position: 'absolute',
            width: 10,
            height: 10
        },
        '& .react-resizable-handle-s': {
            bottom: 0,
            left: '10%',
            width: '80%',
            height: 10,
            cursor: 's-resize',
        },
        '& .react-resizable-handle-n': {
            top: 0,
            left: '10%',
            width: '80%',
            cursor: 'n-resize',
        },
        '& .react-resizable-handle-w': {
            top: '10%',
            left: 0,
            height: '80%',
            cursor: 'w-resize',
        },
        '& .react-resizable-handle-e': {
            top: '10%',
            right: 0,
            height: '80%',
            cursor: 'e-resize',
        },
        '& .react-resizable-handle-sw': {
            bottom: 0,
            left: 0,
            cursor: 'sw-resize',
        },
        '& .react-resizable-handle-se': {
            bottom: 0,
            right: 0,
            cursor: 'se-resize',
        },
        '& .react-resizable-handle-nw': {
            top: 0,
            left: 0,
            cursor: 'nw-resize'
        },
        '& .react-resizable-handle-ne': {
            top: 0,
            right: 0,
            cursor: 'ne-resize'
        }
    },
});

export default OmDialogStyles;
