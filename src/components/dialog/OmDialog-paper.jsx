import React, { memo, useEffect, useState } from 'react';
import { Paper } from '@material-ui/core';
import PropTypes from 'prop-types';
import Draggable from 'react-draggable';
import { Resizable } from 'react-resizable';
import withStyles from '@material-ui/core/styles/withStyles';
import OmDialogStyles from './OmDialogStyles';

const OmDialogPaperComponent = (props) => {
    const { classes, size, initialHeight, initialWidth, ...rest } = props;

    const [dimensions, setDimensions] = useState({
        width: 960,
        height: 600,
    });

    useEffect(() => {
        switch (size) {
            case 'xl':
                setDimensions({width: initialWidth || 1920, height: initialHeight || 700});
                break;
            case 'lg':
                setDimensions({width: initialWidth || 1280, height: initialHeight || 700});
                break;
            case 'md':
                setDimensions({width: initialWidth || 960, height: initialHeight || 600});
                break;
            case 'sm':
                setDimensions({width: initialWidth || 600, height: initialHeight || 600});
                break;
            case 'xs':
                setDimensions({width: initialWidth || 444, height: initialHeight || 600});
                break;
            default:
                setDimensions({width: initialWidth || 960, height: initialHeight || 600});
                break;
        }
    }, [size, initialHeight, initialWidth])

    const onResize = (event, { element, size }) => {
        setDimensions({ width: size.width, height: size.height });
    };

    return (
        <Draggable
            handle="#draggable-dialog-title"
            cancel={'[class*="MuiDialogContent-root"]'}
        >
            <Resizable
                className={classes.resizable}
                width={dimensions.width}
                height={dimensions.height}
                resizeHandles={[
                    's',
                    'w',
                    'e',
                    'n',
                    'sw',
                    'nw',
                    'se',
                    'ne',
                ]}
                onResize={onResize}
            >
                <Paper
                    {...rest}
                    style={{
                        margin: 0,
                        width: dimensions.width + 'px',
                        height: dimensions.height + 'px',
                    }}
                />
            </Resizable>
        </Draggable>
    );
};

OmDialogPaperComponent.defaultProps = {
    classes: undefined,
};

OmDialogPaperComponent.propTypes = {
    classes: PropTypes.object,
};

export default withStyles(OmDialogStyles)(
    memo(OmDialogPaperComponent),
);
