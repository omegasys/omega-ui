import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import DialogContentText from '@material-ui/core/DialogContentText';
import OmDialog from './OmDialog';

export default {
    title: 'Dialog/Dialog',
    component: OmDialog,
};

const Template = (args) => {
    const [open, setOpen] = useState(false);
    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
    };
    const handleCancel = () => {
        setOpen(false);
    };
    const handleConfirm = () => {
        setOpen(false);
    };
    return (
        <>
            <Button variant="outlined" onClick={handleClickOpen}>
                Open Dialog
            </Button>
            <OmDialog
                open={open}
                onConfirm={handleConfirm}
                onCancel={handleCancel}
                onClose={handleClose}
                {...args}
            >
                <DialogContentText>
                    Some random text here
                </DialogContentText>
            </OmDialog>
        </>
    );
};

export const Default = Template.bind({});
Default.args = {
    title: 'Simple Dialog',
};
