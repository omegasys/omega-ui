import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import {
    GetApp,
    GridOn,
    LibraryBooks,
    PictureAsPdf,
} from '@material-ui/icons';
import { Fab } from '@material-ui/core';

// Note: this component is not generic now, will refactor it next week.

const StyledMenu = withStyles({
    paper: {
        border: '1px solid #d3d4d5',
    },
})((props) => (
    <Menu
        elevation={0}
        getContentAnchorEl={null}
        anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
        }}
        transformOrigin={{
            vertical: 'top',
            horizontal: 'center',
        }}
        {...props}
    />
));

const StyledMenuItem = withStyles((theme) => ({
    root: {
        '&:focus': {
            backgroundColor: theme.palette.primary.main,
            '& .MuiListItemIcon-root, & .MuiListItemText-primary': {
                color: theme.palette.common.white,
            },
        },
    },
}))(MenuItem);

function OmDropDown(props) {
    const { onClick, disabled } = props;

    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleItemClick = (event) => (data) => {
        onClick(event);
        handleClose();
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <>
            <Fab
                size="small"
                color="secondary"
                onMouseOver={handleClick}
                disabled={disabled}
            >
                <GetApp />
            </Fab>

            <StyledMenu
                id="customized-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
            >
                <StyledMenuItem onClick={handleItemClick('PDF')}>
                    <ListItemIcon>
                        <PictureAsPdf fontSize="small" />
                    </ListItemIcon>
                    <ListItemText primary="PDF" />
                </StyledMenuItem>
                <StyledMenuItem onClick={handleItemClick('XLSX')}>
                    <ListItemIcon>
                        <GridOn fontSize="small" />
                    </ListItemIcon>
                    <ListItemText primary="XLSX" />
                </StyledMenuItem>
                <StyledMenuItem onClick={handleItemClick('CSV')}>
                    <ListItemIcon>
                        <LibraryBooks fontSize="small" />
                    </ListItemIcon>
                    <ListItemText primary="CSV" />
                </StyledMenuItem>
            </StyledMenu>
        </>
    );
}

OmDropDown.propTypes = {
    onClick: PropTypes.func,
    disabled: PropTypes.bool,
};

OmDropDown.defaultProps = {
    disabled: false,
};

export default React.memo(OmDropDown);
