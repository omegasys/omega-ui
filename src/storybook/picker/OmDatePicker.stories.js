import React, { useState } from 'react';
import OmDatePicker, {
    OmDatePicker as OmDatePickerComponent,
} from '../../components/datetime-picker/OmDatePicker';

export default {
    title: 'Picker/Date Picker',
    component: OmDatePickerComponent,
};

const currentDate = () => new Date();

export const All = () => {
    const [selectedDate, setSelectedDate] = useState(currentDate());
    return (
        <div className="om-grid om-grid-cols-1 om-gap-6">
            <div className="om-flex om-flex-row om-space-x-6">
                <OmDatePicker
                    label="Inline"
                    value={selectedDate}
                    handleDateChange={setSelectedDate}
                    InputProps={{ name: 'lalla' }}
                />
                <OmDatePicker
                    label="Dialog"
                    variant="dialog"
                    value={selectedDate}
                    handleDateChange={setSelectedDate}
                />
            </div>
            <div className="om-flex om-flex-row om-space-x-6">
                <OmDatePicker
                    label="Without icon"
                    value={selectedDate}
                    handleDateChange={setSelectedDate}
                    disabledIcon
                />
            </div>
            <div className="om-flex om-flex-row om-space-x-6">
                <OmDatePicker
                    label="Custom format"
                    format="dd/MM/yyyy"
                    value={selectedDate}
                    handleDateChange={setSelectedDate}
                />
            </div>
            <div className="om-flex om-flex-row om-space-x-6">
                <OmDatePicker
                    label="Disabled"
                    disabled
                    value={selectedDate}
                    handleDateChange={setSelectedDate}
                />
            </div>
        </div>
    );
};
