import React, { useState } from 'react';
import OmTimePicker, {
    OmTimePicker as OmTimePickerComponent,
} from '../../components/datetime-picker/OmTimePicker';

export default {
    title: 'Picker/Time Picker',
    component: OmTimePickerComponent,
};

const currentDate = () => new Date();

export const All = () => {
    const [selectedDate, setSelectedDate] = useState(currentDate());
    return (
        <div className="om-grid om-grid-cols-1 om-gap-6">
            <div className="om-flex om-flex-row om-space-x-6">
                <OmTimePicker
                    label="inline"
                    value={selectedDate}
                    handleDateChange={setSelectedDate}
                />
                <OmTimePicker
                    label="dialog"
                    variant="dialog"
                    value={selectedDate}
                    handleDateChange={setSelectedDate}
                />
            </div>
            <div className="om-flex om-flex-row om-space-x-6">
                <OmTimePicker
                    label="Without icon"
                    value={selectedDate}
                    handleDateChange={setSelectedDate}
                    disabledIcon
                />
            </div>
            <div className="om-flex om-flex-row om-space-x-6">
                <OmTimePicker
                    label="12 hour"
                    ampm
                    value={selectedDate}
                    handleDateChange={setSelectedDate}
                />
                <OmTimePicker
                    label="24 hour"
                    value={selectedDate}
                    handleDateChange={setSelectedDate}
                />
            </div>
            <div className="om-flex om-flex-row om-space-x-6">
                <OmTimePicker
                    label="With seconds"
                    views={['hours', 'minutes', 'seconds']}
                    format="HH:mm:ss"
                    value={selectedDate}
                    handleDateChange={setSelectedDate}
                />
                <OmTimePicker
                    label="Minutes and seconds"
                    openTo="minutes"
                    views={['minutes', 'seconds']}
                    format="mm:ss"
                    value={selectedDate}
                    handleDateChange={setSelectedDate}
                />
            </div>
        </div>
    );
};
