import React, { useState } from 'react';
import OmDateTimePicker, {
    OmDateTimePicker as OmDateTimePickerComponent,
} from '../../components/datetime-picker/OmDateTimePicker';

export default {
    title: 'Picker/DateTime Picker',
    component: OmDateTimePickerComponent,
};

const currentDate = () => new Date();

export const All = () => {
    const [selectedDate, setSelectedDate] = useState(currentDate());
    return (
        <div className="om-grid om-grid-cols-1 om-gap-6">
            <div className="om-flex om-flex-row om-space-x-6">
                <OmDateTimePicker
                    label="inline"
                    value={selectedDate}
                    handleDateChange={setSelectedDate}
                />
                <OmDateTimePicker
                    label="dialog"
                    variant="dialog"
                    value={selectedDate}
                    handleDateChange={setSelectedDate}
                />
            </div>
            <div className="om-flex om-flex-row om-space-x-6">
                <OmDateTimePicker
                    label="Without icon"
                    value={selectedDate}
                    handleDateChange={setSelectedDate}
                    disabledIcon
                />
            </div>
            <div className="om-flex om-flex-row om-space-x-6">
                <OmDateTimePicker
                    label="Custom format"
                    format="yyyy/MM/dd hh:mm a"
                    value={selectedDate}
                    handleDateChange={setSelectedDate}
                />
            </div>
        </div>
    );
};
