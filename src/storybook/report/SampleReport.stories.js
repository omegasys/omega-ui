import React, { useState } from 'react';
import { useForm } from 'react-hook-form';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import OmSimplePage from '../../components/page/OmSimplePage';
import OmButton from '../../components/button/OmButton';
import OmDatePicker from '../../components/datetime-picker/OmDatePicker';
import OmMultiSelect from '../../components/select/OmMultiSelect';
import OmDialog from '../../components/dialog/OmDialog';
import useDialog from '../../hooks/useDialog';
import OmDropzone from '../../components/input/dragNDrop/OmDropzone';
import OmDataTable from '../../components/table/OmDataTable';

export default {
    title: 'Report/Sample Report',
    decorators: [
        (Story) => (
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <Story />
            </MuiPickersUtilsProvider>
        ),
    ],
};

const createData = (
    id,
    date,
    noOfRecords,
    invalidDeposits,
    processedDeposits,
    successfulDeposits,
    failedDeposits,
    staff,
) => ({
    id,
    date,
    noOfRecords,
    invalidDeposits,
    processedDeposits,
    successfulDeposits,
    failedDeposits,
    staff,
});

const columns = [
    { accessor: 'id', Header: 'ID' },
    { accessor: 'date', Header: 'Date' },
    { accessor: 'noOfRecords', Header: 'No of Records' },
    { accessor: 'invalidDeposits', Header: 'Invalid Deposits' },
    { accessor: 'processedDeposits', Header: 'Processed Deposits' },
    { accessor: 'successfulDeposits', Header: 'Successful Deposits' },
    { accessor: 'failedDeposits', Header: 'Failed Deposits' },
    { accessor: 'staff', Header: 'Staff' },
];

const mockData = [
    createData(
        1,
        new Date().toDateString(),
        1000,
        2,
        998,
        700,
        298,
        'Jim Godsell',
    ),
    createData(
        1,
        new Date().toDateString(),
        1000,
        2,
        998,
        700,
        298,
        'Duarte Dias',
    ),
    createData(
        1,
        new Date().toDateString(),
        1000,
        2,
        998,
        700,
        298,
        'Zheng Chen',
    ),
];

const mockCurrentDate = () => new Date();

const SampleTableCriteria = (props) => {
    const { handleSubmit, register, setValue } = useForm();
    const [selectedBrand] = useState();
    const [selectedStartDate, setSelectedStartDate] = useState(
        mockCurrentDate(),
    );
    const [selectedEndDate, setSelectedEndDate] = useState(
        mockCurrentDate(),
    );

    const onSubmit = (data) => {
        console.log(data);
    };

    const handleBrandSelect = (data) => {
        setValue('multiSelectBrands', data);
    };

    React.useEffect(() => {
        register('multiSelectBrands');
    }, [register]);

    return (
        <div className="om-flex om-flex-wrap om-items-center">
            <div className="om-w-full om-p-2 md:om-w-1/3">
                <OmMultiSelect
                    label="Brand"
                    options={[
                        { label: 'Brand 1' },
                        { label: 'Brand 2' },
                        { label: 'Brand 3' },
                    ]}
                    field="label"
                    onChange={handleBrandSelect}
                    name="multiSelectBrands"
                    value={selectedBrand}
                />
            </div>
            <div className="om-w-1/2 om-p-2 md:om-w-1/6">
                <OmDatePicker
                    label="Start Date"
                    value={selectedStartDate}
                    handleDateChange={setSelectedStartDate}
                    name="startDate"
                    inputRef={register}
                />
            </div>
            <div className="om-w-1/2 om-p-2 md:om-w-1/6">
                <OmDatePicker
                    label="End Date"
                    value={selectedEndDate}
                    handleDateChange={setSelectedEndDate}
                    name="endDate"
                    inputRef={register}
                />
            </div>
            <div className="om-p-2">
                <OmButton
                    onClick={handleSubmit(onSubmit)}
                    name="Filter"
                    variant="contained"
                />
            </div>
        </div>
    );
};

export const Default = () => {
    const dialogImport = useDialog();

    const handleDialogImportConfirm = () => {
        console.log('Dialog Confirm!');
        dialogImport.onClose();
    };

    const handleOnDrop = (files) => {
        console.log(files);
    };

    const renderImportDialog = () => (
        <OmDialog
            open={dialogImport.open}
            onConfirm={handleDialogImportConfirm}
            onClose={dialogImport.onClose}
            onCancel={dialogImport.onClose}
        >
            <OmDropzone onDrop={handleOnDrop} />
        </OmDialog>
    );

    return (
        <OmSimplePage>
            <div className="om-flex om-flex-col om-space-y-6">
                <SampleTableCriteria />
                <OmDataTable
                    title="Sample Report"
                    columns={columns}
                    data={mockData}
                    handleAddAction={dialogImport.onOpen}
                />
                {renderImportDialog()}
            </div>
        </OmSimplePage>
    );
};
