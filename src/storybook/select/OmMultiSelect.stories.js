import React from 'react';
import OmMultiSelect, {
    OmMultiSelect as OmMultiSelectComponent,
} from '../../components/select/OmMultiSelect';

export default {
    title: 'Multi Select',
    component: OmMultiSelectComponent,
};

const Template = (args, { loaded: { brands, selectedBrands } }) => (
    <div className="om-grid om-grid-cols-1 om-gap-6 om-w-1/5">
        <OmMultiSelect
            {...args}
            options={brands}
            value={selectedBrands}
        />
    </div>
);

export const Default = Template.bind({});
Default.args = {
    label: 'Options',
    key: 'id',
    displayField: 'label',
    onChange: () => {},
};
