import React from 'react';
import OmSelect, {
    OmSelect as OmSelectComponent,
} from '../../components/select/OmSelect';

export default {
    title: 'Select',
    component: OmSelectComponent,
};

const Template = (args, { loaded: { brands, selectedBrands } }) => (
    <div className="om-grid om-grid-cols-1 om-gap-6 om-w-1/5">
        <OmSelect
            {...args}
            options={brands}
            // value={selectedBrands}
        />
    </div>
);

export const SingleSelect = Template.bind({});
SingleSelect.args = {
    label: 'Options',
    optionKey: 'id',
    optionLabel: 'label',
    onChange: () => {},
};

export const MultiSelect = Template.bind({});
MultiSelect.args = {
    label: 'Options',
    multiple: true,
    optionKey: 'id',
    optionLabel: 'label',
    onChange: () => {},
};
