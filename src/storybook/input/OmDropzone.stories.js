import React from 'react';
import OmDropzone, {
    OmDropzone as OmDropzoneComponent,
} from '../../components/input/dragNDrop/OmDropzone';

export default {
    title: 'Input/Drag n Drop Input',
    component: OmDropzoneComponent,
    // decorators: [(storyFn) => <OmTheme themeName="default">{storyFn()}</OmTheme>],
};

export const All = () => (
    <div className="om-grid om-grid-cols-1 om-gap-6">
        <>
            {SimpleDrop()}
            {DropAcceptingSpecificFileType()}
            {DropAcceptingSpecificFileSize()}
        </>
    </div>
);

const SimpleDrop = () => {
    const handleOnDrop = (files) => {
        console.log(files);
    };

    return (
        <div>
            <p>Simple Drag and Drop with multiple files</p>
            <OmDropzone onDrop={handleOnDrop} />
        </div>
    );
};

const DropAcceptingSpecificFileType = () => {
    const handleOnDrop = (files) => {
        console.log(files);
    };

    return (
        <div>
            <p>Simple Drag and Drop accepting specific file types</p>
            <OmDropzone onDrop={handleOnDrop} accept={['image/*']} />
        </div>
    );
};

const DropAcceptingSpecificFileSize = () => {
    const handleOnDrop = (files) => {
        console.log(files);
    };

    const MIN_FILE_SIZE_BYTE = 1000000; // 1 mb
    const MAX_FILE_SIZE_BYTE = 100000000; // 100 mb
    return (
        <div>
            <p>Simple Drag and Drop accepting specific file size</p>
            <OmDropzone
                onDrop={handleOnDrop}
                minSize={MIN_FILE_SIZE_BYTE}
                maxSize={MAX_FILE_SIZE_BYTE}
            />
        </div>
    );
};
