import React from 'react';
import OmTheme from '../../components/theme/OmTheme';
import OmSearchField from '../../components/input/searchField/OmSearchField';

export default {
    title: 'Input/Search Field',
    // decorators: [(storyFn) => <OmTheme themeName="default">{storyFn()}</OmTheme>],
};

export const All = () => <div>{SimpleSearchField()}</div>;

const SimpleSearchField = () => <OmSearchField />;
