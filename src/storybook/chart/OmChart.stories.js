import React from 'react';
import OmChart, {
    OmChart as OmChartComponent,
} from '../../components/chart/OmChart';
import '../../assets/tailwind.css';
import { withKnobs, select } from '@storybook/addon-knobs';

export default {
    title: 'Chart',
    component: OmChartComponent,
    decorators: [withKnobs],
};

const singleBar = {
    xAxis: {
        legend: 'Day',
        labels: [
            '01/05',
            '02/05',
            '03/05',
            '04/05',
            '05/05',
            '06/05',
            '07/05',
        ],
    },
    data: [
        {
            legend: 'Revenue',
            type: 'bar',
            format: 'currency',
            symbol: '$',
            values: [2300, 1850, 5900, 6210, 2500, 4000, 9500],
        },
    ],
};

const doubleBar = {
    xAxis: {
        legend: 'Day',
        labels: [
            '01/05',
            '02/05',
            '03/05',
            '04/05',
            '05/05',
            '06/05',
            '07/05',
        ],
    },
    data: [
        {
            legend: 'Revenue',
            type: 'bar',
            format: 'currency',
            symbol: '$',
            values: [2300, 1850, 5900, 6210, 2500, 4000, 9500],
        },
        {
            legend: 'Hold',
            type: 'bar',
            format: 'percentage',
            values: [51, 65, 40, 49, 60, 37, 40],
        },
    ],
};

const mixedBar = {
    xAxis: {
        legend: 'Day',
        labels: [
            '01/05',
            '02/05',
            '03/05',
            '04/05',
            '05/05',
            '06/05',
            '07/05',
        ],
    },
    data: [
        {
            legend: 'Revenue',
            type: 'bar',
            format: 'currency',
            symbol: '$',
            values: [2300, 1850, 5900, 6210, 2500, 4000, 9500],
            order: 1,
        },
        {
            legend: 'Hold',
            type: 'line',
            format: 'percentage',
            values: [51, 65, 40, 49, 60, 37, 40],
            order: 0,
        },
    ],
};

const mockLine = {
    labels: [
        '01/05',
        '02/05',
        '03/05',
        '04/05',
        '05/05',
        '06/05',
        '07/05',
    ],
    datasets: [
        {
            label: 'dataset',
            fill: false,
            lineTension: 0,
            borderColor: '#ABE1A0',
            backgroundColor: '#ABE1A0',
            pointBorderColor: '#ABE1A0',
            pointBackgroundColor: '#ABE1A0',
            pointBorderWidth: 11,
            pointHoverBorderWidth: 11,
            pointHoverBackgroundColor: '#ABE1A0',
            pointHoverBorderColor: '#ABE1A0',
            data: [65, 59, 80, 81, 56, 55, 40],
        },
    ],
};

const mockPieData = {
    labels: ['Red', 'Green', 'Yellow'],
    datasets: [
        {
            data: [300, 50, 100],
            backgroundColor: ['#FF6384', '#36A2EB', '#FFCE56'],
            hoverBackgroundColor: ['#FF6384', '#36A2EB', '#FFCE56'],
        },
    ],
};

export const Bar = () => {
    let data;
    const type = select(
        'Chart Type',
        ['Single Set', 'Double Set', 'Mixed Set'],
        'Single Set',
    );
    switch (type) {
        case 'Single Set':
            data = singleBar;
            break;
        case 'Double Set':
            data = doubleBar;
            break;
        case 'Mixed Set':
            data = mixedBar;
            break;
    }
    return <OmChart variant="bar" data={data} />;
};

export const Line = () => <OmChart variant="line" data={mockLine} />;

export const Pie = () => <OmChart variant="pie" data={mockPieData} />;
