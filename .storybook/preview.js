import { addParameters } from '@storybook/react';
import {
  DocsPage,
  DocsContainer,
} from '@storybook/addon-docs/blocks';
import {
  DEFAULT_VIEWPORT,
  MINIMAL_VIEWPORTS,
} from '@storybook/addon-viewport';
import React from 'react';
import OmTheme from '../src/components/theme/OmTheme';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import fetch from 'node-fetch';

const withThemeProvider = (Story, context) => {
  return (
    <OmTheme themeName={context.globals.theme}>
      <Story />
    </OmTheme>
  );
};

export const decorators = [
  (Story) => (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <Story />
    </MuiPickersUtilsProvider>
  ),
  withThemeProvider,
];

export const globalTypes = {
  theme: {
    name: 'Theme',
    description: 'Global theme for components',
    defaultValue: 'default',
    toolbar: {
      icon: 'circlehollow',
      // array of plain string values or MenuItem shape (see below)
      items: ['default', 'sunset'],
    },
  },
};

export const loaders = [
  async () => ({
    brands: await fetch('data/data.json')
      .then((res) => res.json())
      .then((dataSet) => dataSet.brands),
    selectedBrands: await fetch('data/data.json')
      .then((res) => res.json())
      .then((dataSet) => dataSet.selectedBrands),
    movies: await fetch('data/data.json')
      .then((res) => res.json())
      .then((dataSet) => dataSet.movies),
  }),
];

addParameters({
  docs: {
    container: DocsContainer,
    page: DocsPage,
  },
  viewport: {
    viewports: MINIMAL_VIEWPORTS,
    defaultViewport: DEFAULT_VIEWPORT,
  },
});
