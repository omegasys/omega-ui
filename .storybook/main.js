module.exports = {
    stories: [
        // '../src/stories/**/*.stories.@(js|jsx)', 
        '../src/**/*.stories.@(js|jsx)',
        '../src/storybook/**/*.stories.@(js|jsx)'
    ],
    addons: [
        '@storybook/addon-docs',
        '@storybook/addon-essentials',
        '@storybook/addon-links',
        '@storybook/addon-knobs',
        '@storybook/addon-viewport'
    ]
};
